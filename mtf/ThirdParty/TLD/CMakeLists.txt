# Open The CMake GUI
# specify the source directory and the binary directory
# press configure
# set CMAKE_INSTALL_PREFIX to the path where you want to install the program
# press configure
# check BUILD_WITH_QT if you want to build the program with a QT-Config GUI
# check GENERATE_DEB_PACKAGE if you want to build a debian package (only on Linux)
#
# UNIX Makefile:
# 1) go to the binary folder and type "make" to build the project
# 2) (optional) type "make install all" to install the files into the install
#    directory
# 3) (optional) type "make package" to create a package of the install folder
#    (.tgz file if GENERATE_DEB_PACKAGE=false, .deb file if GENERATE_DEB_PACKAGE=true)
#
# Microsoft Visual C++:
# 1) open the .sln file
# 2) change the mode to "Release" -> only Release is supported!
# 3) build the project "ALL_BUILD" to build the opentld project
# 4) build the project "INSTALL" to install the files into the install
#    directory
# 5) build the project "PACKAGE" to create an NSIS-installer (NSIS is required)


project(OpenTLD)

cmake_minimum_required(VERSION 2.6)
set(TLD_LIB_INSTALL_DIR /usr/local/lib CACHE PATH "Directory to install OpenTLD tracker library")

find_package(OpenCV REQUIRED)
add_subdirectory(3rdparty/cvblobs)
add_library(opentld SHARED
    src/mftracker/BB.cpp
    src/mftracker/BBPredict.cpp
    src/mftracker/FBTrack.cpp
    src/mftracker/Lk.cpp
    src/mftracker/Median.cpp
    src/Clustering.cpp
    src/DetectionResult.cpp
    src/DetectorCascade.cpp
    src/EnsembleClassifier.cpp
    src/ForegroundDetector.cpp
    src/MedianFlowTracker.cpp
    src/NNClassifier.cpp
    src/TLDUtil.cpp
    src/VarianceFilter.cpp)	
target_link_libraries(opentld ${OpenCV_LIBS} cvblobs)
target_include_directories(opentld PUBLIC include 3rdparty/cvblobs ${OpenCV_INCLUDE_DIRS})
target_link_libraries(opentld ${OpenCV_LIB_DIR})
# set_target_properties(opentld PROPERTIES OUTPUT_NAME opentld)
install(TARGETS opentld LIBRARY DESTINATION ${TLD_LIB_INSTALL_DIR})
