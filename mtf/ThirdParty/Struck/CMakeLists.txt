project(struck)

cmake_minimum_required(VERSION 2.6)

find_package(OpenCV REQUIRED)
find_package(Eigen3 REQUIRED)

if(NOT CMAKE_BUILD_TYPE)
    set(CMAKE_BUILD_TYPE Release CACHE STRING "Build type" FORCE)
endif()

INCLUDE_DIRECTORIES (include ${OpenCV_INCLUDE_DIRS} ${EIGEN3_INCLUDE_DIR} )

add_library (struck SHARED
    src/Config.cpp
    src/Features.cpp
    src/HaarFeature.cpp
    src/HaarFeatures.cpp
    src/HistogramFeatures.cpp
    src/ImageRep.cpp
    src/LaRank.cpp
    src/MultiFeatures.cpp
    src/RawFeatures.cpp
    src/Sampler.cpp
    src/Tracker.cpp
    src/GraphUtils/GraphUtils.cpp
    )
set_target_properties(struck PROPERTIES COMPILE_FLAGS "-Wfatal-errors -Wno-write-strings -O3  -std=c++11")
target_link_libraries(struck ${OpenCV_LIBS})
target_include_directories(struck PUBLIC include ${OpenCV_INCLUDE_DIRS} ${EIGEN3_INCLUDE_DIR})
set(STRUCK_INSTALL_DIR /usr/local/lib CACHE PATH "Directory to install Struck tracker library")
install(TARGETS struck LIBRARY DESTINATION ${STRUCK_INSTALL_DIR})