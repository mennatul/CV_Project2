#include "mtf/mtf.h"

// tools for capturing images from disk or cameras
#include "mtf/Tools/inputCV.h"
#ifndef DISABLE_XVISION
#include "mtf/Tools/inputXV.h"
#endif
#ifndef DISABLE_VISP
#include "mtf/Tools/inputVP.h"
#endif
// tools for preprocessing the image
#include "mtf/Tools/PreProc.h"
// general OpenCV utilities for selecting objects, reading ground truth, etc.
#include "mtf/Tools/cvUtils.h"
// parameters for different modules
#include "mtf/Config/parameters.h"

#ifdef _WIN32
#define snprintf  _snprintf
#include <time.h>
#else
#include <ctime> 
#endif
#include <string.h>
#include <numeric>
#include <vector>
#include <iomanip>

#include "opencv2/core/core.hpp"
#include "opencv2/highgui/highgui.hpp"

#include "boost/filesystem/operations.hpp"
#include "boost/filesystem/path.hpp"

#define MAX_FPS 1e6

#ifdef _WIN32
#define start_input_timer() \
	clock_t start_time_with_input = clock()
#define start_tracking_timer() \
	clock_t start_time = clock()
#define end_both_timers(fps, fps_win) \
	clock_t end_time = clock();\
	fps = CLOCKS_PER_SEC / static_cast<double>(end_time - start_time);\
	fps_win = CLOCKS_PER_SEC / static_cast<double>(end_time - start_time_with_input)
#else
#define start_input_timer() \
	timespec start_time_with_input;\
	clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &start_time_with_input)
#define start_tracking_timer() \
	timespec start_time;\
	clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &start_time)
#define end_both_timers(fps, fps_win) \
	timespec end_time;\
	clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &end_time);\
	fps = 1.0 / ((double)(end_time.tv_sec - start_time.tv_sec) +\
		1e-9*(double)(end_time.tv_nsec - start_time.tv_nsec));\
	fps_win = 1.0 / ((double)(end_time.tv_sec - start_time_with_input.tv_sec) +\
		1e-9*(double)(end_time.tv_nsec - start_time_with_input.tv_nsec))
#endif

#define OPENCV_PIPELINE 'c'
#ifndef DISABLE_XVISION
#define XVISION_PIPELINE 'x'
#endif
#ifndef DISABLE_VISP
#define VISP_PIPELINE 'v'
#endif
using namespace std;
using namespace mtf::params;
namespace fs = boost::filesystem;

int main(int argc, char * argv[]) {
	printf("Starting MTF...\n");
	if(enable_nt){
		printf("Using non templated search methods\n");
	}

	// *************************************************************************************************** //
	// ********************************** read configuration parameters ********************************** //
	// *************************************************************************************************** //

	// check if a custom configuration directory has been specified
	if(argc > 2 && !strcmp(argv[1], "config_dir")){
		config_dir = string(argv[2]);
		printf("Reading configuration files from: %s\n", config_dir.c_str());
	}
	vector<char*> fargv;
	// read general parameters
	int fargc = readParams(fargv, (config_dir + "/mtf.cfg").c_str());
	if(fargc){
		if(!parseArgumentPairs(fargv.data(), fargc)){
			printf("Error in parsing mtf.cfg\n");
			return EXIT_FAILURE;
		}
		fargv.clear();
	}
	// read parameters specific to different modules
	fargc = readParams(fargv, (config_dir + "/modules.cfg").c_str());
	if(fargc){
		if(!parseArgumentPairs(fargv.data(), fargc)){
			printf("Error in parsing modules.cfg\n");
			return EXIT_FAILURE;
		}
		fargv.clear();
	}
	// parse command line arguments
	if(!parseArgumentPairs(argv, argc, 1, 1)){
		printf("Error in parsing command line arguments\n");
		return EXIT_FAILURE;
	}
#ifdef ENABLE_PARALLEL
	Eigen::initParallel();
#endif
	printf("*******************************\n");
	printf("Using parameters:\n");
	printf("n_trackers: %d\n", n_trackers);
	printf("actor_id: %d\n", actor_id);
	printf("source_id: %d\n", source_id);
	printf("source_name: %s\n", source_name.c_str());
	printf("actor: %s\n", actor.c_str());
	printf("pipeline: %c\n", pipeline);
	printf("img_source: %c\n", img_source);
	printf("show_cv_window: %d\n", show_cv_window);
	printf("read_obj_from_gt: %d\n", read_obj_from_gt);
	printf("write_tracking_data: %d\n", write_tracking_data);
	printf("mtf_sm: %s\n", mtf_sm);
	printf("mtf_am: %s\n", mtf_am);
	printf("mtf_ssm: %s\n", mtf_ssm);
	printf("*******************************\n");

	// *********************************************************************************************** //
	// ********************************** initialize input pipeline ********************************** //
	// *********************************************************************************************** //

	InputBase *input = nullptr;
	if(pipeline == OPENCV_PIPELINE) {
		input = new InputCV(img_source, source_name, source_fmt, source_path, buffer_count);
	}
#ifndef DISABLE_XVISION
	else if(pipeline == XVISION_PIPELINE) {
		input = new InputXV(img_source, source_name, source_fmt, source_path, buffer_count);
	}
#endif
#ifndef DISABLE_VISP
	else if(pipeline == VISP_PIPELINE) {
		input = new InputVP(
			img_source, source_name, source_fmt, source_path, buffer_count, visp_usb_n_buffers,
			static_cast<VpResUSB>(visp_usb_res), static_cast<VpFpsUSB>(visp_usb_fps),
			static_cast<VpResFW>(visp_fw_res), static_cast<VpFpsFW>(visp_fw_fps)
			);
	}
#endif
	else {
		printf("Invalid video pipeline provided: %c\n", pipeline);
		return EXIT_FAILURE;
	}
	if(!input->initialize()){
		printf("Pipeline could not be initialized successfully. Exiting...\n");
		return EXIT_FAILURE;
	}

	// **************************************************************************************************** //
	// ********************************** initialize image pre processor ********************************** //
	// **************************************************************************************************** //

	PreProc *pre_proc = nullptr;
	if(pre_proc_type == "-1" || pre_proc_type == "none"){
		pre_proc = new NoProcessing();
	} else if(pre_proc_type == "0" || pre_proc_type == "gauss"){
		pre_proc = new GaussianSmoothing(gauss_kernel_size, gauss_sigma_x, gauss_sigma_y);
	} else if(pre_proc_type == "1" || pre_proc_type == "med"){
		pre_proc = new MedianFiltering(med_kernel_size);
	} else if(pre_proc_type == "2" || pre_proc_type == "box"){
		pre_proc = new NormalizedBoxFltering(box_kernel_size);
	} else if(pre_proc_type == "3" || pre_proc_type == "bil"){
		pre_proc = new BilateralFiltering(bil_diameter, bil_sigma_col, bil_sigma_space);
	} else{
		printf("Invalid image pre processing type specified: %s\n", pre_proc_type.c_str());
		return EXIT_FAILURE;
	}
	pre_proc->initialize(input->getFrame());

	//printf("done getting no. of frames\n");
	printf("n_frames=%d\n", input->n_frames);

	if(init_frame_id > 0){
		printf("Skipping %d frames...\n", init_frame_id);
	}
	for(int frame_id = 0; frame_id < init_frame_id; frame_id++){
		if(!input->update()){
			printf("Frame %d could not be read from the input pipeline", input->getFameID() + 1);
			return EXIT_FAILURE;
		}
	}
	pre_proc->update(input->getFrame()); 

	// **************************************************************************************************** //
	// ******************* get objects to be tracked and read ground truth if available ******************* //
	// **************************************************************************************************** //

	bool init_obj_read = false;
	CVUtils cv_utils;
	vector<obj_struct*> init_objects;
	if(read_obj_from_gt){
		obj_struct* init_object = cv_utils.readObjectFromGT(source_name, source_path, input->n_frames,
			init_frame_id, debug_mode, use_opt_gt, opt_gt_ssm);
		if(init_object){
			init_obj_read = true;
			for(int tracker_id = 0; tracker_id < n_trackers; tracker_id++) {
				init_objects.push_back(init_object);
			}
		} else{
			printf("Failed to read initial object from ground truth; using manual selection...\n");
		}
	}
	if(!init_obj_read && read_obj_from_file) {
		init_objects = cv_utils.readObjectsFromFile(n_trackers, read_obj_fname, debug_mode);
		if(init_objects[0]){
			init_obj_read = true;
		} else{
			printf("Failed to read initial object location from file; using manual selection...\n");
		}
	}
	if(!init_obj_read){
		if(img_source == SRC_IMG || img_source == SRC_DISK || img_source == SRC_VID){
			//printf("calling getMultipleObjects with sel_quad_obj=%d\n", sel_quad_obj);
			init_objects = cv_utils.getMultipleObjects(input->getFrame(), n_trackers,
				patch_size, line_thickness, write_objs, sel_quad_obj, write_obj_fname);
			//printf("done calling getMultipleObjects\n");
		} else{
			//printf("calling getMultipleObjects with sel_quad_obj=%d\n", sel_quad_obj);
			init_objects = cv_utils.getMultipleObjects(input, n_trackers,
				patch_size, line_thickness, write_objs, sel_quad_obj, write_obj_fname);
			//printf("done calling getMultipleObjects\n");
		}
	}
	if(reinit_from_gt){
		if(!cv_utils.readReinitGT(source_name, source_path, input->n_frames,
			debug_mode, use_opt_gt, read_from_bin, opt_gt_ssm)){
			printf("Reinitialization ground truth could not be read.\n");
			return EXIT_FAILURE;
		}
		printf("Tracker reinitialization on failure is enabled with error threshold: %f and skipped frames: %d\n",
			reinit_err_thresh, reinit_frame_skip);
	}

	// *************************************************************************************************** //
	// *************************************** initialize trackers *************************************** //
	// *************************************************************************************************** //

	if(n_trackers > 1){
		printf("Multi tracker setup enabled\n");
		write_tracking_data = 0;
		reinit_from_gt = 0;
		show_ground_truth = 0;
	}
	if(res_from_size){
		printf("Getting sampling resolution from object size...\n");
	}
	
	FILE *multi_fid = nullptr;
	bool pre_proc_enabled = false;
	vector<mtf::TrackerBase*> trackers;
	for(int tracker_id = 0; tracker_id < n_trackers; tracker_id++) {
		if(n_trackers > 1){
			multi_fid = readTrackerParams(multi_fid);
		}
		printf("Initializing tracker %d with object of size %f x %f\n", tracker_id,
			init_objects[tracker_id]->size_x, init_objects[tracker_id]->size_y);
		if(res_from_size){
			resx = init_objects[tracker_id]->size_x;
			resy = init_objects[tracker_id]->size_y;
		}
		mtf::TrackerBase *new_tracker;
		if(enable_nt){
			// Non Templated variant
			new_tracker = mtf::getSM(mtf_sm, mtf_am, mtf_ssm);
		} else{
			new_tracker = mtf::getTrackerObj(mtf_sm, mtf_am, mtf_ssm);
		}
		if(!new_tracker){
			printf("Tracker could not be initialized successfully\n");
			return EXIT_FAILURE;
		}
		if(new_tracker->inputType() == CV_32FC1){
			new_tracker->setImage(pre_proc->getFrame());	
			pre_proc_enabled = true;
		} else if(new_tracker->inputType() == CV_8UC3){
			new_tracker->setImage(input->getFrame());
		} else if(new_tracker->inputType() == HETEROGENEOUS_INPUT){
			new_tracker->setImage(pre_proc->getFrame());
			new_tracker->setImage(input->getFrame());
			pre_proc_enabled = true;
		}
		new_tracker->initialize(init_objects[tracker_id]->corners);
		trackers.push_back(new_tracker);
	}

	// ******************************************************************************************** //
	// *************************************** setup output *************************************** //
	// ******************************************************************************************** //

	string cv_win_name = "OpenCV Window :: " + source_name;
	string proc_win_name = "Processed Image :: " + source_name;
	if(show_cv_window) {
		cv::namedWindow(cv_win_name, cv::WINDOW_AUTOSIZE);
		if(show_proc_img) {
			cv::namedWindow(proc_win_name, cv::WINDOW_AUTOSIZE);
		}
	}
	// if reading object from ground truth did not succeed then the 
	// ground truth is invalid and computing tracking error is not possible 
	// nor is it possible to reinitialize the object from ground truth
	show_tracking_error = show_tracking_error && read_obj_from_gt;
	reinit_from_gt = reinit_from_gt && read_obj_from_gt;
	show_ground_truth = show_ground_truth && read_obj_from_gt;

	if(show_tracking_error || reinit_from_gt){
		int valid_gt_frames = cv_utils.ground_truth.size();
		if(input->n_frames <= 0 || valid_gt_frames < input->n_frames){
			if(input->n_frames <= 0){
				printf("Disabling tracking error computation\n");
			} else if(valid_gt_frames>0){
				printf("Disabling tracking error computation since ground truth is only available for %d out of %d frames\n",
					valid_gt_frames, input->n_frames);
			} else{
				printf("Disabling tracking error computation since ground truth is not available\n");
			}
			show_tracking_error = 0;
			reinit_from_gt = 0;
			show_ground_truth = 0;
		}
	}
	cv::VideoWriter output;
	if(record_frames){
		output.open("Tracked_video.avi", CV_FOURCC('M', 'J', 'P', 'G'), 24, input->getFrame().size());
	}
	FILE *tracking_data_fid = nullptr;
	if(write_tracking_data){
		char tracking_data_dir[500];
		char tracking_data_path[500];

		if(reinit_from_gt){
			snprintf(tracking_data_dir, 500, "log/tracking_data/reinit_%d_%d/%s/%s",
				static_cast<int>(reinit_err_thresh), reinit_frame_skip, actor.c_str(), source_name.c_str());
		} else{
			snprintf(tracking_data_dir, 500, "log/tracking_data/%s/%s", actor.c_str(), source_name.c_str());
		}

		if(!fs::exists(tracking_data_dir)){
			printf("Tracking data directory: %s does not exist. Creating it...\n", tracking_data_dir);
			fs::create_directories(tracking_data_dir);
		}
		if(!tracking_data_fname){
			tracking_data_fname = new char[500];
			snprintf(tracking_data_fname, 500, "%s_%s_%s_%d", mtf_sm, mtf_am, mtf_ssm, 1 - hom_normalized_init);
		}
		snprintf(tracking_data_path, 500, "%s/%s.txt", tracking_data_dir, tracking_data_fname);
		printf("Writing tracking data to: %s\n", tracking_data_path);
		tracking_data_fid = fopen(tracking_data_path, "w");
		fprintf(tracking_data_fid, "frame ulx uly urx ury lrx lry llx lly\n");
	}

	double fps = 0, fps_win = 0;
	double avg_fps = 0, avg_fps_win = 0;
	int fps_count = 0;
	double avg_err = 0;

	cv::Point fps_origin(10, 20);
	double fps_font_size = 0.50;
	cv::Scalar fps_color(0, 255, 0);
	cv::Point err_origin(10, 40);
	double err_font_size = 0.50;
	cv::Scalar err_color(0, 255, 0);
	cv::Scalar gt_color(0, 255, 0);

	if(reset_template){
		printf("Template resetting is enabled\n");
	}

	double tracking_err = 0;
	int failure_count = 0;
	bool is_initialized = true;
	int valid_frame_count = 0;
	int reinit_frame_id = 0;
	cv::Point2d corners[4];

	// ********************************************************************************************** //
	// *************************************** start tracking ! ************************************* //
	// ********************************************************************************************** //

	while(true) {

		// *********************** compute tracking error and reinitialize if needed *********************** //

		if(show_tracking_error || reinit_from_gt){
			//printf("computing error for frame: %d\n", frame_id - 1);	
			cv::Mat gt_corners;
			if(reinit_from_gt){
				gt_corners = cv_utils.reinit_ground_truth[reinit_frame_id][input->getFameID() - reinit_frame_id];
			} else{
				gt_corners = cv_utils.ground_truth[input->getFameID()];
			}
			cv::Mat tracker_corners = trackers[0]->getRegion();
			// compute the mean corner distance between the tracking result and the ground truth
			tracking_err = 0;
			for(int corner_id = 0; corner_id < 4; ++corner_id){
				double x_diff = gt_corners.at<double>(0, corner_id) - tracker_corners.at<double>(0, corner_id);
				double y_diff = gt_corners.at<double>(1, corner_id) - tracker_corners.at<double>(1, corner_id);
				//printf("x_diff: %f\n", x_diff);
				//printf("y_diff: %f\n", y_diff);
				tracking_err += sqrt((x_diff*x_diff) + (y_diff*y_diff));
			}
			tracking_err /= 4.0;
			//printf("tracking_err: %f\n", tracking_err);
			//printf("done computing error\n");
			if(reinit_from_gt && tracking_err > reinit_err_thresh){
				++failure_count;
				printf("Tracking failure %4d detected in frame %5d with error: %10.6f. ",
					failure_count, input->getFameID() + 1, tracking_err);
				if(write_tracking_data){
					fprintf(tracking_data_fid, "frame%05d.jpg tracker_failed\n", input->getFameID() + 1);
				}
				if(input->getFameID() + reinit_frame_skip >= input->n_frames){
					printf("Reinitialization is not possible as insufficient frames (%d) are left to skip. Exiting...\n",
						input->n_frames - input->getFameID() - 1);
					break;
				}
				bool skip_success = true;
				for(int skip_id = 0; skip_id < reinit_frame_skip; ++skip_id) {
					if(!input->update()){
						printf("Frame %d could not be read from the input pipeline", input->getFameID() + 1);
						skip_success = false;
						break;
					}
				}
				if(!skip_success){ break; }
				printf("Reinitializing in frame %5d...\n", input->getFameID() + 1);
				//cout << "Using ground truth:\n" << cv_utils.ground_truth[input->getFameID()] << "\n";
				if(pre_proc_enabled){
					pre_proc->update(input->getFrame());
				}
				reinit_frame_id = input->getFameID();
				trackers[0]->initialize(cv_utils.reinit_ground_truth[reinit_frame_id][0]);
				is_initialized = true;
			}
			if(is_initialized){
				is_initialized = false;
			} else{// exclude initialization frames for computing the average error				
				if(!std::isinf(fps)){
					++valid_frame_count;
					avg_err += (tracking_err - avg_err) / valid_frame_count;
					//fps_vector.push_back(fps);	
				}
			}
		}

		// *************************** display/save tracking output *************************** //

		if(record_frames || show_cv_window) {
			/* draw tracker positions to OpenCV window */
			for(int tracker_id = 0; tracker_id < n_trackers; ++tracker_id) {				
				int col_id = tracker_id % cv_utils.no_of_cols;
				cv_utils.cornersToPoint2D(corners, trackers[tracker_id]->getRegion());
				line(input->getFrameMutable(), corners[0], corners[1], cv_utils.obj_cols[col_id], line_thickness);
				line(input->getFrameMutable(), corners[1], corners[2], cv_utils.obj_cols[col_id], line_thickness);
				line(input->getFrameMutable(), corners[2], corners[3], cv_utils.obj_cols[col_id], line_thickness);
				line(input->getFrameMutable(), corners[3], corners[0], cv_utils.obj_cols[col_id], line_thickness);
				putText(input->getFrameMutable(), trackers[tracker_id]->name, corners[0],
					cv::FONT_HERSHEY_SIMPLEX, fps_font_size, cv_utils.obj_cols[col_id]);
			}
			if(show_ground_truth){			
				cv_utils.cornersToPoint2D(corners, cv_utils.ground_truth[input->getFameID()]);
				line(input->getFrameMutable(), corners[0], corners[1], gt_color, line_thickness);
				line(input->getFrameMutable(), corners[1], corners[2], gt_color, line_thickness);
				line(input->getFrameMutable(), corners[2], corners[3], gt_color, line_thickness);
				line(input->getFrameMutable(), corners[3], corners[0], gt_color, line_thickness);
				putText(input->getFrameMutable(), "ground_truth", corners[0],
					cv::FONT_HERSHEY_SIMPLEX, fps_font_size, gt_color);
			}
			char fps_text[100];
			snprintf(fps_text, 100, "frame: %d c: %9.3f a: %9.3f cw: %9.3f aw: %9.3f fps",
				input->getFameID() + 1, fps, avg_fps, fps_win, avg_fps_win);
			putText(input->getFrameMutable(), fps_text, fps_origin, cv::FONT_HERSHEY_SIMPLEX, fps_font_size, fps_color);

			if(show_tracking_error){
				char err_text[100];
				snprintf(err_text, 100, "ce: %12.8f ae: %12.8f", tracking_err, avg_err);
				putText(input->getFrameMutable(), err_text, err_origin, cv::FONT_HERSHEY_SIMPLEX, err_font_size, err_color);
			}

			if(record_frames){
				output.write(input->getFrame());
			}
			if(show_cv_window){
				imshow(cv_win_name, input->getFrame());
				if(show_proc_img){
					cv::Mat  proc_img_uchar(pre_proc->getFrame().rows, pre_proc->getFrame().cols, CV_8UC1);
					pre_proc->getFrame().convertTo(proc_img_uchar, proc_img_uchar.type());
					imshow(proc_win_name, proc_img_uchar);
				}
				int pressed_key = cv::waitKey(1 - pause_after_frame);
				if(pressed_key%256 ==27){//27){
					break;
				}
				if(pressed_key%256 == 32){
					pause_after_frame = 1 - pause_after_frame;
				}
			}
		}
		if(!show_cv_window && (input->getFameID() + 1) % 50 == 0){
			printf("frame_id: %5d avg_fps: %15.9f avg_fps_win: %15.9f avg_err: %15.9f\n",
				input->getFameID() + 1, avg_fps, avg_fps_win, avg_err);
		}
		if(write_tracking_data){
			fprintf(tracking_data_fid, "frame%05d.jpg %15.6f %15.6f %15.6f %15.6f %15.6f %15.6f %15.6f %15.6f\n", input->getFameID() + 1,
				trackers[0]->getRegion().at<double>(0, 0), trackers[0]->getRegion().at<double>(1, 0),
				trackers[0]->getRegion().at<double>(0, 1), trackers[0]->getRegion().at<double>(1, 1),
				trackers[0]->getRegion().at<double>(0, 2), trackers[0]->getRegion().at<double>(1, 2),
				trackers[0]->getRegion().at<double>(0, 3), trackers[0]->getRegion().at<double>(1, 3));
		}

		if(input->n_frames > 0 && input->getFameID() >= input->n_frames - 1){
			printf("==========End of input stream reached==========\n");
			break;
		}

		// ******************************* update pipeline and trackers ******************************* //

		start_input_timer();
		// update pipeline
		if (!input->update()){
			printf("Frame %d could not be read from the input pipeline", input->getFameID() + 1);
			break;
		}
		if(pre_proc_enabled){
			// update pre processor
			pre_proc->update(input->getFrame());
		}
		//update trackers		
		for(int tracker_id = 0; tracker_id < n_trackers; ++tracker_id) {
			start_tracking_timer();
			trackers[tracker_id]->update();// this is equivalent to :				
			// trackers[tracker_id]->update(pre_proc->getFrame()); 			
			// as the image has been passed at the time of initialization 
			// and does not need to be passed again as long as the new 
			// image is read into the same locatioon
			end_both_timers(fps, fps_win);
			if(reset_template){
				trackers[tracker_id]->initialize(trackers[tracker_id]->getRegion());
			}
		}
		if(!std::isinf(fps) && fps<MAX_FPS){
			++fps_count;
			avg_fps += (fps - avg_fps) / fps_count;
			// if fps is not inf then fps_win too must be non inf
			avg_fps_win += (fps_win - avg_fps_win) / fps_count;
		}
	}
	cv::destroyAllWindows();

	//double avg_fps = accumulate( fps_vector.begin(), fps_vector.end(), 0.0 )/ fps_vector.size();
	//double avg_fps_win = accumulate( fps_win_vector.begin(), fps_win_vector.end(), 0.0 )/ fps_win_vector.size();
	printf("Average FPS: %15.10f\n", avg_fps);
	printf("Average FPS with Input: %15.10f\n", avg_fps_win);
	if(show_tracking_error){
		printf("Average Tracking Error: %15.10f\n", avg_err);
		printf("Frames used for computing the average: %d\n", valid_frame_count);
	}
	if(reinit_from_gt){
		printf("Number of failures: %d\n", failure_count);
	}
	if(write_tracking_data){
		fclose(tracking_data_fid);
		FILE *tracking_stats_fid = fopen("log/tracking_stats.txt", "a");
		fprintf(tracking_stats_fid, "%s\t %s\t %s\t %s\t %d\t %s\t %15.9f\t %15.9f",
			source_name.c_str(), mtf_sm, mtf_am, mtf_ssm, hom_normalized_init, tracking_data_fname, avg_fps, avg_fps_win);
		if(show_tracking_error){
			fprintf(tracking_stats_fid, "\t %15.9f", avg_err);
		}
		if(reinit_from_gt){
			fprintf(tracking_stats_fid, "\t %d", failure_count);
		}
		fprintf(tracking_stats_fid, "\n");
		fclose(tracking_stats_fid);
	}
	if(record_frames){
		output.release();
	}
	//delete(input);
	//delete(pre_proc);
	//trackers.clear();
	return EXIT_SUCCESS;
}
