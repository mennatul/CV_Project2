#ifdef _WIN32
#define snprintf  _snprintf
#endif

#include "mtf/Test/mtf_test.h"

#include "mtf/Config/parameters.h"
#include "mtf/Config//datasets.h"

#include "mtf/Tools/cvUtils.h"
#include "mtf/Tools/inputCV.h"
#ifndef DISABLE_XVISION
#include "mtf/Tools/inputXV.h"
#endif
// tools for preprocessing the image
#include "mtf/Tools/PreProc.h"

#include <time.h>
#include <string.h>
#include <numeric>
#include <vector>
#include <iomanip>

#include "opencv2/core/core.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"

#include "boost/filesystem/operations.hpp"
#include "boost/filesystem/path.hpp"

#ifndef DISABLE_XVISION
#define XVISION_PIPELINE 'x'
#endif
#define OPENCV_PIPELINE 'c'

using namespace std;
namespace fs = boost::filesystem;
using namespace mtf::params;

mtf::DiagBase *diag;
VectorXd param_range;
typedef mtf::DiagBase::AnalyticalDataType ADT;
typedef mtf::DiagBase::NumericalDataType NDT;

const char* getDataTypeName(int data_id, int adt_len, int diag_len){
	if(data_id<adt_len){// analytical
		return diag->getADTName(static_cast<ADT>(data_id));
	} else if(data_id<diag_len - 1){// numerical
		return diag->getNDTName(static_cast<NDT>(data_id-adt_len));
	} else if(data_id == diag_len - 1){// ssm
		return "ssm_param";
	} else{
		throw std::invalid_argument("getDataTypeName:: Invalid datay type provided");
	}
	
}

void generateData(int data_id, int adt_len, int diag_len){
	char *out_fname = nullptr;
	if(data_id < adt_len){// analytical
		if(diag_verbose){
			printf("Generating %s data\t",
				diag->getADTName(static_cast<ADT>(data_id)));
		}
		diag->generateAnalyticalData(param_range, diag_res, static_cast<ADT>(data_id), out_fname);
	} else if(data_id < diag_len - 1){// numerical
		int num_data_id = data_id - adt_len;
		//printf("data_id: %d\n", data_id);
		//printf("adt_len: %d\n", adt_len);
		//printf("num_data_id: %d\n", num_data_id);
		if(diag_verbose){
			printf("Generating numerical %s data\t",
				diag->getNDTName(static_cast<NDT>(num_data_id)));
		}
		diag->generateNumericalData(param_range, diag_res,
			static_cast<NDT>(num_data_id), out_fname, diag_grad_diff);
	} else if(data_id == diag_len - 1){// ssm
		if(diag_verbose){ printf("Generating SSMParam data\t"); }
		diag->generateSSMParamData(param_range, diag_res, out_fname);
	} else{
		printf("Data type: %d\n", data_id);
		throw std::invalid_argument("generateData:: Invalid data type provided");
	}
}

void generateInverseData(int data_id, int adt_len, int diag_len){
	char *out_fname = nullptr;	
	if(data_id<adt_len){// analytical
		if(diag_verbose){ printf("Generating inverse %s data\t", diag->getADTName(static_cast<ADT>(data_id))); }
		diag->generateInverseAnalyticalData(param_range, diag_res, static_cast<ADT>(data_id), out_fname);
	} else if(data_id<diag_len-1){// numerical
		if(diag_verbose){ printf("Generating numerical inverse %s data\t", 
			diag->getNDTName(static_cast<NDT>(data_id - adt_len)));
		}
		diag->generateInverseNumericalData(param_range, diag_res, static_cast<NDT>(data_id - adt_len), out_fname, diag_grad_diff);
	} else if(data_id == diag_len-1){// ssm
		if(diag_verbose){ printf("Generating SSMParam data\t"); }
		diag->generateSSMParamData(param_range, diag_res, out_fname);
	} else{
		printf("Data type: %d\n", data_id);
		throw std::invalid_argument("generateInverseData:: Invalid data type provided");
	}
}

int main(int argc, char * argv[]) {

	// check if a custom configuration directory has been specified
	if(argc > 2 && !strcmp(argv[1], "config_dir")){
		config_dir = string(argv[2]);
		printf("Reading configuration files from: %s\n", config_dir.c_str());
	}
	vector<char*> fargv;
	// read general parameters
	int fargc = readParams(fargv, (config_dir + "/mtf.cfg").c_str());
	if(fargc){
		if(!parseArgumentPairs(fargv.data(), fargc)){
			printf("Error in parsing mtf.cfg\n");
			return EXIT_FAILURE;
		}
		fargv.clear();
	}
	// read parameters specific to different modules
	fargc = readParams(fargv, (config_dir + "/modules.cfg").c_str());
	if(fargc){
		if(!parseArgumentPairs(fargv.data(), fargc)){
			printf("Error in parsing modules.cfg\n");
			return EXIT_FAILURE;
		}
		fargv.clear();
	}
	// parse command line arguments
	if(!parseArgumentPairs(argv, argc, 1, 1)){
		printf("Error in parsing command line arguments\n");
		return EXIT_FAILURE;
	}

	cout << "*******************************\n";
	cout << "Using parameters:\n";
	cout << "source_id: " << source_id << "\n";
	cout << "source_name: " << source_name << "\n";
	cout << "actor: " << actor << "\n";
	cout << "steps_per_frame: " << steps_per_frame << "\n";
	cout << "pipeline: " << pipeline << "\n";
	cout << "img_source: " << img_source << "\n";
	cout << "show_cv_window: " << show_cv_window << "\n";
	cout << "read_obj_from_file: " << read_obj_from_file << "\n";
	cout << "record_frames: " << record_frames << "\n";
	cout << "patch_size: " << patch_size << "\n";
	cout << "read_obj_fname: " << read_obj_fname << "\n";
	cout << "read_obj_from_gt: " << read_obj_from_gt << "\n";
	cout << "show_warped_img: " << show_warped_img << "\n";
	cout << "pause_after_frame: " << pause_after_frame << "\n";
	cout << "write_tracker_states: " << write_tracker_states << "\n";
	cout << "*******************************\n";

	InputBase *input = nullptr;

	cv::Point fps_origin(10, 20);
	double fps_font_size = 0.50;
	cv::Scalar fps_color(0, 255, 0);
	char fps_text[100];

	cv::Point err_origin(10, 40);
	double err_font_size = 0.50;
	cv::Scalar err_color(0, 255, 0);
	char err_text[100];

	/* initialize pipeline*/
	if(pipeline == OPENCV_PIPELINE) {
		input = new InputCV(img_source, source_name, source_fmt, source_path, buffer_count);
	}
#ifndef DISABLE_XVISION
	else if(pipeline == XVISION_PIPELINE) {
		input = new InputXV(img_source, source_name, source_fmt, source_path, buffer_count);
	}
#endif
	else {
		printf("Invalid video pipeline provided\n");
		return EXIT_FAILURE;
	}
	if(!input->initialize()){
		printf("Pipeline could not be initialized successfully\n");
		return EXIT_FAILURE;
	}
	/* initialize frame pre processor*/
	PreProc *pre_proc = nullptr;
	if(pre_proc_type == "-1" || pre_proc_type == "none"){
		pre_proc = new NoProcessing();
	} else if(pre_proc_type == "0" || pre_proc_type == "gauss"){
		pre_proc = new GaussianSmoothing(gauss_kernel_size, gauss_sigma_x, gauss_sigma_y);
	} else if(pre_proc_type == "1" || pre_proc_type == "med"){
		pre_proc = new MedianFiltering(med_kernel_size);
	} else if(pre_proc_type == "2" || pre_proc_type == "box"){
		pre_proc = new NormalizedBoxFltering(box_kernel_size);
	} else if(pre_proc_type == "3" || pre_proc_type == "bil"){
		pre_proc = new BilateralFiltering(bil_diameter, bil_sigma_col, bil_sigma_space);
	} else{
		printf("Invalid image pre processing type specified: %s\n", pre_proc_type.c_str());
		return EXIT_FAILURE;
	}
	pre_proc->initialize(input->getFrame());


	//printf("done getting no. of frames\n");
	printf("n_frames=%d\n", input->n_frames);

	for(int i = 0; i < init_frame_id; i++){
		if(!input->update()){
			printf("Frame %d could not be read from the input pipeline", input->getFameID() + 1);
			return EXIT_FAILURE;
		}
	}
	pre_proc->update(input->getFrame());

	bool init_obj_read = false;
	/*get objects to be tracked*/
	CVUtils *util_obj = new CVUtils;
	vector<obj_struct*> init_objects;
	if(read_obj_from_gt){
		obj_struct* init_object = util_obj->readObjectFromGT(source_name, source_path, input->n_frames, init_frame_id, debug_mode);
		if(init_object){
			init_obj_read = true;
			for(int i = 0; i < n_trackers; i++) {
				init_objects.push_back(init_object);
			}
		} else{
			printf("Failed to read initial object location from ground truth; using manual selection...\n");
		}

	}
	if(!init_obj_read && read_obj_from_file) {
		init_objects = util_obj->readObjectsFromFile(n_trackers, read_obj_fname, debug_mode);
		if(init_objects[0]){
			init_obj_read = true;
		} else{
			printf("Failed to read initial object location from file; using manual selection...\n");
		}
	}
	if(!init_obj_read){
		//printf("calling getMultipleObjects\n");
		init_objects = util_obj->getMultipleObjects(input->getFrame(), n_trackers,
			patch_size, line_thickness, write_objs, write_obj_fname);
		//printf("done calling getMultipleObjects\n");
	}


	/*********************************** initialize trackers ***********************************/

	printf("Initializing diag with object of size %f x %f\n",
		init_objects[0]->size_x, init_objects[0]->size_y);
	if(res_from_size){
		resx = init_objects[0]->size_x;
		resy = init_objects[0]->size_y;
	}
	diag = mtf::getDiagnosticsObj(diag_am, diag_ssm);
	if(!diag){
		printf("Diagnostics could not be initialized successfully\n");
		exit(0);
	}
	//char feat_norm_fname[100], norm_fname[100], jac_fname[100], hess_fname[100], hess2_fname[100];
	//char jac_num_fname[100], hess_num_fname[100], nhess_num_fname[100];
	//char ssm_fname[100];
	char bin_out_fname[100];
	char diag_data_dir[500];	

	snprintf(diag_data_dir, 500, "log/diagnostics/%s", source_name.c_str());
	if(!fs::exists(diag_data_dir)){
		printf("Diagnostic data directory: %s does not exist. Creating it...\n", diag_data_dir);
		fs::create_directories(diag_data_dir);
	}
	diag->setImage(pre_proc->getFrame());
	diag->initialize(init_objects[0]->corners);

	param_range.resize(diag->ssm_state_size);
	if(diag_range){
		param_range.fill(diag_range);
	} else{
		if(diag_ssm_range.size()<diag->ssm_state_size){
			throw std::invalid_argument("testMTF:: Insufficient number of SSM range parameters provided");
		}
		
		for(int state_id = 0; state_id < diag->ssm_state_size; ++state_id){
			param_range[state_id] = diag_ssm_range[state_id];
		}
	}
	if(res_from_size){
		printf("Getting sampling resolution from object size...\n");
	}

	char diag_gen[100];
	strcpy(diag_gen, diag_gen_norm);
	strcat(diag_gen, diag_gen_jac);
	strcat(diag_gen, diag_gen_hess);
	strcat(diag_gen, diag_gen_hess2);
	strcat(diag_gen, diag_gen_hess_sum);
	strcat(diag_gen, diag_gen_num);
	strcat(diag_gen, diag_gen_ssm);

	int diag_len = strlen(diag_gen);
	int adt_len = strlen(diag_gen_norm) + strlen(diag_gen_jac) + 
		strlen(diag_gen_hess) + strlen(diag_gen_hess2) + strlen(diag_gen_hess_sum);

	printf("diag_gen_norm: %s\n", diag_gen_norm);
	printf("diag_gen_jac: %s\n", diag_gen_jac);
	printf("diag_gen_hess: %s\n", diag_gen_hess);
	printf("diag_gen_hess2: %s\n", diag_gen_hess2);
	printf("diag_gen_hess_sum: %s\n", diag_gen_hess_sum);
	printf("diag_gen_num: %s\n", diag_gen_num);
	printf("diag_gen_ssm: %s\n", diag_gen_ssm);
	printf("diag_gen: %s\n", diag_gen);
	printf("diag_len: %d\n", diag_len);
	printf("adt_len: %d\n", adt_len);
	printf("diag_frame_gap: %d\n", diag_frame_gap);

	int start_id = init_frame_id >= diag_frame_gap ? init_frame_id : diag_frame_gap;
	int end_id = end_frame_id >= init_frame_id ? end_frame_id : input->n_frames - 1;
	if(end_id>start_id){
		printf("Generating diagnostics data for frames %d to %d\n", start_id, end_id);
	} else {
		end_id = start_id;
		printf("Generating diagnostics data for frame %d\n", start_id);
	}

	ofstream out_files[diag_len];
	if(diag_bin){
		for(int data_id = 0; data_id < diag_len; data_id++){
			if(diag_gen[data_id] - '0'){
				const char* data_name = getDataTypeName(data_id, adt_len, diag_len);
				if(diag_inv){
					snprintf(bin_out_fname, 100, "%s/%s_%s_%d_inv_%s_%d_%d_%d.bin",
						diag_data_dir, diag_am, diag_ssm, diag_update, data_name, 
						diag_frame_gap, start_id, end_id);
					printf("Writing inv_%s data to %s\n", data_name, bin_out_fname);
				} else{
					snprintf(bin_out_fname, 100, "%s/%s_%s_%d_%s_%d_%d_%d.bin",
						diag_data_dir, diag_am, diag_ssm, diag_update, data_name, 
						diag_frame_gap, start_id, end_id);
					printf("Writing %s data to %s\n", data_name, bin_out_fname);
				}

				out_files[data_id].open(bin_out_fname, ios::out | ios::binary);
				out_files[data_id].write((char*)(&diag_res), sizeof(int));
				out_files[data_id].write((char*)(&(diag->ssm_state_size)), sizeof(int));
			}
		}
	} 

	for(int i = init_frame_id; i < start_id; i++){
		if(!input->update()){
			printf("Frame %d could not be read from the input pipeline", input->getFameID() + 1);
			return EXIT_FAILURE;
		}
	}
	pre_proc->update(input->getFrame());

	printf("frame_id: %d\n", input->getFameID());

	for(int frame_id = start_id; frame_id <= end_id; frame_id++){
		printf("Processing frame: %d\n", frame_id);
		//update diagnostics module
		diag->update(util_obj->ground_truth[frame_id - diag_frame_gap]);

		for(int data_id = 0; data_id < diag_len; data_id++){
			if(diag_gen[data_id] - '0'){
				clock_t start_time = clock();
				if(diag_inv){
					generateInverseData(data_id, adt_len, diag_len);
				} else{
					generateData(data_id, adt_len, diag_len);
				}
				clock_t end_time = clock();
				double time_taken = static_cast<double>(end_time - start_time) / CLOCKS_PER_SEC;
				if(diag_verbose){
					printf("Time taken:\t %f\n", time_taken);
				}

				out_files[data_id].write((char*)(diag->diagnostics_data.data()),
					sizeof(double)*diag->diagnostics_data.size());
			}
		}
		if(diag_verbose){
			printf("***********************************\n");
		}
		// update frame
		if(!input->update()){
			printf("Frame %d could not be read from the input pipeline", input->getFameID() + 1);
			return EXIT_FAILURE;
		}
		pre_proc->update(input->getFrame());
	}
	printf("Closing files...\n");
	if(diag_bin){
		for(int data_id = 0; data_id < diag_len; data_id++){
			if(diag_gen[data_id] - '0'){
				out_files[data_id].close();
			}
		}		
	}
	printf("Done closing files\n");
	return EXIT_SUCCESS;
}
