#include "inputBase.h"
InputBase::InputBase(char img_source, char* dev_name_in, char *dev_fmt_in,
	char* dev_path_in, int n_buffers){

	init_success = 0;
	n_channels = 3;

	xvision_code(
		n_channels = sizeof(PIX_TYPE);
	);
	dev_name = dev_name_in;
	dev_fmt = dev_fmt_in;
	dev_path = dev_path_in;

	this->n_buffers = n_buffers;

	fprintf(stdout, "Starting InputBase constructor\n");
	fprintf(stdout, "img_source=%c\n", img_source);

	/*fprintf(stdout, "before: dev_name=%d\n", dev_name);
	fprintf(stdout, "before: dev_fmt=%d\n", dev_fmt);*/

	frames_captured = 0;
	n_frames = 0;

	switch(img_source) {
	case SRC_VID: {
		if(!dev_fmt){
			dev_fmt = static_cast<char*>(VID_FMT);
		}
		if(!dev_name){
			dev_name = static_cast<char*>(VID_FNAME);
		}
		if(!dev_path){
			dev_path = static_cast<char*>(ROOT_FOLDER);
		}
		snprintf(file_path, MAX_PATH_SIZE, "%s/%s.%s", dev_path, dev_name, dev_fmt);
		break;
	}
	case SRC_IMG: {
		if(!dev_fmt){
			dev_fmt = static_cast<char*>(IMG_FMT);
		}
		if(!dev_name){
			dev_name = static_cast<char*>(IMG_FOLDER);
		}
		if(!dev_path){
			dev_path = static_cast<char*>(ROOT_FOLDER);
		}
		snprintf(file_path, MAX_PATH_SIZE, "%s/%s/frame%%05d.%s", dev_path, dev_name, dev_fmt);
		n_frames = getNumberOfFrames(file_path);
		break;
	}
	case SRC_USB_CAM: {
		if(!dev_fmt){
			dev_fmt = static_cast<char*>(USB_DEV_FORMAT);
		}
		if(!dev_name){
			dev_name = static_cast<char*>(USB_DEV_NAME);
		}
		if(!dev_path){
			dev_path = static_cast<char*>(USB_DEV_PATH);
		}
		break;
	}
	case SRC_DIG_CAM: {
		if(!dev_fmt){
			dev_fmt = static_cast<char*>(DIG_DEV_FORMAT);
		}
		if(!dev_name){
			dev_name = static_cast<char*>(DIG_DEV_NAME);
		}
		if(!dev_path){
			dev_path = static_cast<char*>(DIG_DEV_PATH);
		}
		break;
	}
	default: {
		fprintf(stdout, "Invalid image source provided\n");
		exit(0);
	}
	}
	fprintf(stdout, "dev_name=%s\n", dev_name);
	fprintf(stdout, "dev_path=%s\n", dev_path);
	fprintf(stdout, "dev_fmt=%s\n", dev_fmt);
}
int InputBase::getNumberOfFrames(char *file_template){
	FILE* fid;
	int frame_id = 0;
	char fname[MAX_PATH_SIZE];
	snprintf(fname, MAX_PATH_SIZE, file_template, ++frame_id);
	while(fid = fopen(fname, "r")){
		fclose(fid);
		snprintf(fname, MAX_PATH_SIZE, file_template, ++frame_id);
	}
	printf("Could not find file: %s\n", fname);
	return frame_id - 1;
}

xvision_code(
	void InputBase::copyXVToCV(IMAGE_TYPE_GS &xv_img, cv::Mat *cv_img) {
	//printf("Copying XV to CV\n");
	int img_height = xv_img.SizeY();
	int img_width = xv_img.SizeX();
	//printf("img_height: %d, img_width=%d\n", img_height, img_width);
	/*cv_img=new cv::Mat(img_height, img_width, CV_8UC1);
	printf("created image with img_height: %d, img_width: %d\n",
	cv_img->rows, cv_img->cols);*/
	PIX_TYPE_GS* xv_data = (PIX_TYPE_GS*)xv_img.data();
	uchar* cv_data = cv_img->data;
	for(int row = 0; row < img_height; row++) {
		//printf("row: %d\n", row);
		for(int col = 0; col < img_width; col++) {
			//printf("\tcol: %d\n", col);
			int xv_location = col + row * img_width;
			int cv_location = (col + row * img_width);
			cv_data[cv_location] = (uchar)xv_data[xv_location];
		}
	}
	//printf("done\n");
}

void InputBase::copyXVToCVIter(IMAGE_TYPE_GS &xv_img, cv::Mat *cv_img) {
	int img_width = xv_img.Width();
	int img_height = xv_img.Height();
	//printf("img_height: %d, img_width=%d\n", img_height, img_width);
	int cv_location = 0;
	XVImageIterator<PIX_TYPE_GS> iter(xv_img);
	uchar* cv_data = cv_img->data;
	for(; !iter.end(); ++iter){
		cv_data[cv_location++] = *iter;
	}
}
void InputBase::copyXVToCVIter(IMAGE_TYPE &xv_img, cv::Mat *cv_img) {
	int img_width = xv_img.Width();
	int img_height = xv_img.Height();
	//printf("img_height: %d, img_width=%d\n", img_height, img_width);
	int cv_location = 0;
	XVImageIterator<PIX_TYPE> iter(xv_img);
	uchar* cv_data = cv_img->data;
	for(; !iter.end(); ++iter){
		cv_data[cv_location++] = iter->b;
		cv_data[cv_location++] = iter->g;
		cv_data[cv_location++] = iter->r;
	}
}
)
