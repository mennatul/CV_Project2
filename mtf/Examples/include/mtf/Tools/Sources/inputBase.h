#ifndef _INC_RANDOMINPUTBASE_
#define _INC_RANDOMINPUTBASE_

#include <stdio.h>
#include <stdlib.h>

#ifndef DISABLE_XVISION
#define xvision_code(code) code
#include <XVVideo.h>
#include <XVMpeg.h>
#include <XVAVI.h>
#include <XVV4L2.h>
#include <XVDig1394.h>
#include <XVImageRGB.h>
#include <XVImageSeq.h>
#else
#define xvision_code(code) 
#endif


#include "opencv2/core/core.hpp"
#include "opencv2/highgui/highgui.hpp"

#define NCHANNELS 3

#define SRC_VID 'm'
#define SRC_USB_CAM 'u'
#define SRC_DIG_CAM 'f'
#define SRC_IMG 'j'

#define VID_FNAME "nl_bus"
#define VID_FMT "mpg"

#define IMG_FOLDER "line"
#define IMG_FMT "jpg"

#define ROOT_FOLDER "../../Datasets/Human"

#define DIG_DEV_NAME "firewire_cam"
#define DIG_DEV_PATH "/dev/fw1"
#define DIG_DEV_FORMAT "i1V1f7o3r150"

#define USB_DEV_NAME "usb_cam"
#define USB_DEV_PATH "/dev/video0"
#define USB_DEV_FORMAT "I1B4r0N0"

#define CV_CAM_ID 0

#define MAX_PATH_SIZE 500

	using namespace std;

xvision_code(
	typedef float PIX_TYPE_GS;
typedef XV_RGB24 PIX_TYPE;
typedef XVImageRGB<PIX_TYPE> IMAGE_TYPE;
typedef XVImageScalar<PIX_TYPE_GS> IMAGE_TYPE_GS;
)

class InputBase {

public:
	xvision_code(
		IMAGE_TYPE** xv_buffer = nullptr;
	IMAGE_TYPE* xv_frame = nullptr;
	);
	cv::Mat** cv_buffer = nullptr;
	cv::Mat* cv_frame = nullptr;
	cv::Mat* cv_frame_gs = nullptr;

	int init_success;

	int img_width;
	int img_height;
	int n_buffers;
	int n_channels;
	int buffer_id;

	int n_frames;
	int frames_captured;

	char file_path[MAX_PATH_SIZE];
	char *dev_name = nullptr;
	char *dev_fmt = nullptr;
	char *dev_path = nullptr;

	InputBase(char img_source = SRC_VID, char* dev_name_in = nullptr, char *dev_fmt_in = nullptr,
		char* dev_path_in = nullptr, int n_buffers = 1);
	virtual ~InputBase(){}
	virtual void updateFrame() = 0;
	virtual void remapBuffer(uchar **new_addr) = 0;
	int getNumberOfFrames(char *file_template);
	xvision_code(
		void copyXVToCV(IMAGE_TYPE_GS &xv_img, cv::Mat *cv_img);
	void copyXVToCVIter(IMAGE_TYPE_GS &xv_img, cv::Mat *cv_img);
	)
};
#endif