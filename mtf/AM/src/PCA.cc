#include "mtf/AM/PCA.h"
//#include "mtf/AM/ImageBase.h"
#include <fstream>      // std::ifstream
#include <iostream>     // std::cout
#include <time.h>
#include "opencv2/highgui/highgui.hpp"
_MTF_BEGIN_NAMESPACE

//! value constructor
PCAParams::PCAParams(ImgParams *img_params,
string _basis_data_root_dir,
string _actor,
string _seq_name,
int _extraction_id) :
ImgParams(img_params),
basis_data_root_dir(_basis_data_root_dir),
actor(_actor),
seq_name(_seq_name),
extraction_id(_extraction_id){}
//! default/copy constructor
PCAParams::PCAParams(PCAParams *params) :
ImgParams(params),
basis_data_root_dir(PCA_BASIS_DATA_ROOT_DIR),
actor(PCA_ACTOR),
seq_name(PCA_SEQ_NAME),
extraction_id(PCA_EXTRACTION_ID){
	if(params){
		basis_data_root_dir = params->basis_data_root_dir;
		actor = params->actor;
		seq_name = params->seq_name;
		extraction_id = params->extraction_id;
	}
}

PCA::PCA(ParamType *pca_params) :
SSDBase(pca_params), params(pca_params){
	printf("\n");
	printf("Initializing PCA appearance model with...\n");
	printf("grad_eps: %e\n", grad_eps);
	printf("hess_eps: %e\n", hess_eps);
	name = "pca";
	n_feat = n_pix;
}

void PCA::updateDistFeat(){
	//eigen_feat = eigen_patch.transpose() * getCurrPixVals();
	//std::cout << "The eigen feature is " << eigen_feat << std::endl;
}

const double* PCA::getDistFeat(){
	return getCurrFeat().data();
}

double PCA::getLikelihood(){
	// Use the metric from SSDBase, but normalize over number of features instead of
	// the number of pixels
	double result = exp(-sqrt(-similarity / static_cast<double>(n_pix)));
	//std::cout << "The likelihood is " << result  << std::endl;
	return result;
}

void PCA::initialize() {
	SSDBase::initialize();
	// ****** Read basis patches from files
	//string in_fname = "TMT_nl_mugII_s3_3.bin"; // hard-coded...
	//string in_fname = "TMT_nl_cereal_s3_3.bin";
	string in_fname = cv::format("%s/%s/%s_%dx%d_%d.bin",
		params.basis_data_root_dir.c_str(), 
		params.actor.c_str(), params.seq_name.c_str(),
		params.resx, params.resy, params.extraction_id);
	printf("Reading basis data from: %s\n", in_fname.c_str());
	// Define Eigen Matrix to hold the x frames by 10000 matrix
	ifstream in_file;
	in_file.open(in_fname, ios::in | ios::binary);
	if(in_file.is_open()) {
		printf("Now reading the training patches...\n");
		in_file.seekg(0, in_file.end);
		int length = in_file.tellg();
		in_file.seekg(0, in_file.beg);
		int num_elements = length / sizeof(double);
		std::cout << "Reading " << num_elements << " numbers ... " << std::endl;
		int rows = n_pix;
		int cols = num_elements / rows;
		std::cout << "Number of columns: " << cols << std::endl;
		n_feat = n_feat <= cols ? n_feat : cols;
		MatrixXd extracted_patches(rows, cols);
		// read data as a block:
		in_file.read((char*)(extracted_patches.data()), length);
		in_file.close();

		// Print the data in matrix
		//std::cout << "The extracted patches is: " << extracted_patches.col(0)
		//		<< std::endl;

		// ****** Compute PCA
		// Mean center the data
		printf("PCA: start of computation.\n");
		cout << "The first pixel " << extracted_patches.col(0).topRows(2) << endl;
		mean_img = extracted_patches.rowwise().mean();
		MatrixXd centered = extracted_patches.colwise() - mean_img;
		cout << "The centered patch first 10 numbers: " << centered.col(1).topRows(10) << endl;
		//cout << "The mean image: " << centered.topRows(10) << endl;

		// SVD
		JacobiSVD<MatrixXd> svd(centered, ComputeThinU | ComputeThinV);
		eigen_patch = svd.matrixU().leftCols(n_feat);
		cout << "Its singular values are:" << endl << svd.singularValues() << endl;
		cout << "The eigen patch (first row) is: \n" << eigen_patch.row(0) << endl;

		// Show the eigen_patch as image

		//for(int i = 0; i < eigen_patch.cols(); i++) {
		//	// Construct Mat object for current patch. It points to the memory without copying data
		//	cv::Mat curr_img_cv = cv::Mat(getResY(), getResX(), CV_64FC1,
		//		const_cast<double*>(eigen_patch.col(i).data()));
		//	// Construct Mat object for the uint8 gray scale patch
		//	cv::Mat curr_img_cv_uchar(getResY(), getResX(), CV_8UC1);
		//	// Convert the RGB patch to grayscale
		//	curr_img_cv.convertTo(curr_img_cv_uchar, curr_img_cv_uchar.type());
		//	imshow("Patch", curr_img_cv_uchar);
		//	if(cv::waitKey(0) == 27)
		//		break;
		//}

		// Show the condition numbers of basis
		//JacobiSVD<MatrixXd> svd(pcaTransform);
		//double cond = svd.singularValues()(0) / svd.singularValues()(svd.singularValues().size()-1);
		//cout << "Condition number is: " << cond << endl;
		printf("PCA: end of computation.\n");

	} else {
		printf("PCA Could not open binary file.\n");
	}

	eigen_transform = eigen_patch * eigen_patch.transpose();
	VectorXd current_img = getInitPixVals();
	current_img -= mean_img;
	// compare the time of two matrix multiplication
	//clock_t tStart = clock();
	//init_feat =  current_img - eigen_transform * current_img; // reconstruction error
	//printf("Time taken 1: %.5fs\n", (double)(clock() - tStart)/CLOCKS_PER_SEC);

	//VectorXd tmp1 = init_feat;
	//current_img = getInitPixVals() - mean_img;

	//tStart = clock();
	init_feat = current_img;
	init_feat.noalias() -= eigen_patch * (eigen_patch.transpose() * current_img);
	//printf("Time taken 2: %.5fs\n", (double)(clock() - tStart)/CLOCKS_PER_SEC);
	//VectorXd tmp2 = init_feat;

	//MatrixXd identity = MatrixXd::Identity(2500,2500);
	//tStart = clock();
	//init_feat = (identity - eigen_transform) * current_img;
	//printf("Time taken 3: %.5fs\n", (double)(clock() - tStart)/CLOCKS_PER_SEC);
	//VectorXd tmp3 = init_feat;

	// compare the features learned
	//cout << "feat1 : feat2" << (tmp1 == tmp2) << endl;
	//cout << "feat1 : feat3" << (tmp1 == tmp3) << endl;

	init_norm = init_feat.squaredNorm() / 2;
	//init_feat = eigen_patch.transpose() * getInitPixVals();
	//init_feat = eigen_patch.colPivHouseholderQr().solve(getInitPixVals());
	//init_feat = init_feat / init_feat.sum();
	std::cout << "The init eigen feature is " << init_norm << std::endl;
	//cv::waitKey(0);
}

void PCA::update(bool prereq_only/*=true*/) {
	// Similarity: negative number, the higher the score, the more similar it is
	VectorXd current_img = getCurrPixVals();
	current_img -= mean_img;
	//curr_feat_diff = current_img - eigen_transform * current_img;

	curr_feat_diff = current_img;
	curr_feat_diff.noalias() -= eigen_patch * (eigen_patch.transpose() * current_img);
	//curr_feat = eigen_patch.colPivHouseholderQr().solve(getCurrPixVals());
	//curr_feat = curr_feat / curr_feat.sum();
	//std::cout << "The current eigen feature is " << curr_feat.transpose() << std::endl;
	//curr_feat_diff = curr_feat - init_feat;
	//std::cout << "The current feature diff is " << curr_feat_diff.transpose() << std::endl;
	if(prereq_only){ return; }
	similarity = -curr_feat_diff.squaredNorm();
	similarity = similarity / init_norm * 50;
	//std::cout << "The similarity is " << similarity  << std::endl;
}

/*
void PCA::initializePixVals(const Matrix2Xd& init_pts) {
// Call the function from the base class
ImageBase::initializePixVals(int_pts);
}*/


/*void ImageBase::initializePixVals(const Matrix2Xd& init_pts){
	if(!isInitialized()->pix_vals){
	init_pix_vals.resize(n_pix);
	curr_pix_vals.resize(n_pix);
	}
	++frame_count;
	utils::getPixVals(init_pix_vals, curr_img, init_pts, n_pix,
	img_height, img_width, pix_norm_mult, pix_norm_add);

	if(!isInitialized()->pix_vals){
	curr_pix_vals = init_pix_vals;
	isInitialized()->pix_vals = true;
	}
	}
	*/
_MTF_END_NAMESPACE

