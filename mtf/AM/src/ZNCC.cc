#include "mtf/AM/ZNCC.h"
#include "mtf/Utilities/imgUtils.h"

_MTF_BEGIN_NAMESPACE

ZNCC::ZNCC(ParamType *ncc_params) :
	SSDBase(ncc_params) {
	name = "zncc";
	printf("\n");
	printf("Initializing Zero mean Normalized Cross Correlation AM...\n");
	init_pix_mean = init_pix_var = init_pix_std = 0;
	curr_pix_mean = curr_pix_var = curr_pix_std = 0;
}

void ZNCC::initializePixVals(const Matrix2Xd& curr_pts) {
	if(!isInitialized()->pix_vals){
		init_pix_vals.resize(n_pix);
		curr_pix_vals.resize(n_pix);
	}
	++frame_count;

	utils::getPixVals(init_pix_vals, curr_img, curr_pts, n_pix,
		img_height, img_width);

	init_pix_mean = init_pix_vals.mean();
	init_pix_vals = (init_pix_vals.array() - init_pix_mean);

	init_pix_var = init_pix_vals.squaredNorm() / n_pix;
	init_pix_std = sqrt(init_pix_var);
	init_pix_vals /= init_pix_std;

	pix_norm_mult = 1.0 / init_pix_std;
	pix_norm_add = init_pix_mean;

	if(!is_initialized.pix_vals){
		curr_pix_vals = init_pix_vals;
		curr_pix_mean = init_pix_mean;
		curr_pix_var = init_pix_var;
		curr_pix_std = init_pix_std;
		is_initialized.pix_vals = true;
	}
}

void ZNCC::updatePixVals(const Matrix2Xd& curr_pts) {
	utils::getPixVals(curr_pix_vals, curr_img, curr_pts, n_pix,
		img_height, img_width);

	curr_pix_mean = curr_pix_vals.mean();
	curr_pix_vals = (curr_pix_vals.array() - curr_pix_mean);

	curr_pix_var = curr_pix_vals.squaredNorm() / n_pix;
	curr_pix_std = sqrt(curr_pix_var);
	curr_pix_vals /= curr_pix_std;

	pix_norm_mult = 1.0 / curr_pix_std;
	pix_norm_add = curr_pix_mean;
}

_MTF_END_NAMESPACE

