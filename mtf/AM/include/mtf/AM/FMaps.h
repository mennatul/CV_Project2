#ifndef FMAPs_H
#define FMAPs_H

#include "AppearanceModel.h"
#include <caffe/caffe.hpp>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <algorithm>
#include <iosfwd>
#include <memory>
#include <string>
#include <utility>
#include <vector>
#include <iostream>
#include "SSDBase.h"

#define NFMAPS 10
#define LAYER "conv1"
#define VIS 0
#define EZNCC 1
//#define MODELF "../../../Googlenet_Models/deploy.prototxt"//
#define MODELF "../../../VGG_Models/VGG_deploy.prototxt"
//#define MEANF " ../../../Googlenet_Models/imagenet_mean.binaryproto"//
#define MEANF "../../../VGG_Models/VGG_mean.binaryproto"
//#define PARAMSF "../../../Googlenet_Models/imagenet_googlenet.caffemodel"//
#define PARAMSF "../../../VGG_Models/VGG_CNN_F.caffemodel" 

_MTF_BEGIN_NAMESPACE

using namespace std;
using namespace caffe;  // NOLINT(build/namespaces)
using std::string;
using namespace cv;

struct FMapsParams : ImgParams{
    int nfmaps;
    char* layer_name;
    int vis;
    int e_zncc;

    char *model_file_name;
    char *mean_file_name;
    char *params_file_name;

    //! value constructor
	FMapsParams(ImgParams *img_params,
		int _n_fmaps, char* _layer_name, int _vis, int _zncc, char *_model_f_name, char *_mean_f_name, char *_params_f_name):
		ImgParams(img_params){
            nfmaps= _n_fmaps;
            layer_name= _layer_name;
            vis= _vis;
            e_zncc= _zncc;

            model_file_name= _model_f_name;
            mean_file_name= _mean_f_name;
            params_file_name= _params_f_name;
	}

	//! default/copy constructor
	FMapsParams(FMapsParams *params = nullptr) :
		ImgParams(params),
		nfmaps(NFMAPS), layer_name(LAYER), vis(VIS), e_zncc(EZNCC), model_file_name(MODELF), mean_file_name(MEANF), params_file_name(PARAMSF){
		if(params){
			nfmaps = params->nfmaps;
            layer_name= params->layer_name;
            vis= params->vis;
            e_zncc= params->e_zncc;

            model_file_name= params->model_file_name;
            mean_file_name= params->mean_file_name;
            params_file_name= params->params_file_name;

		}
	}
};


class FMaps: public SSDBase
{
public:

	typedef FMapsParams ParamType; 
    
	static ParamType static_params;
    ParamType params; 
	PixValT init_pix_vals_temp, curr_pix_vals_temp;
    PixValT temp_fmaps;

    int n_pix_temp;
    
    FMaps(ParamType *img_params);
    FMaps();
 	void initializePixVals(const Matrix2Xd& curr_pts) override;
	void updatePixVals(const Matrix2Xd& curr_pts) override;
       
    std::vector<cv::Mat> extract_features(cv::Mat img, char* layer_name);
    
    std::vector<cv::Mat> init_fmaps, curr_fmaps;
	//! mean, variance and standard deviation of the initial pixel values
	double init_pix_mean, init_pix_var, init_pix_std;
	//! mean, variance and standard deviation of the current pixel values
	double curr_pix_mean, curr_pix_var, curr_pix_std;
    
    int nfmaps;
    char* layer_name;
    int vis;
    int e_zncc;
    
private:
    void set_mean(const string& mean_file);
    void wrap_input_layer(std::vector<cv::Mat>* input_channels);
    void preprocess(const cv::Mat& img,std::vector<cv::Mat>* input_channels);
    void wrap_any_layer(std::vector<cv::Mat>* input_channels, boost::shared_ptr< Blob<float> > layer);
    cv::Mat convert_float_img(Mat &img);

private:
    boost::shared_ptr<Net<float> > net_;
    cv::Size input_geometry_;
    int num_channels_;
    cv::Mat mean_;
};

_MTF_END_NAMESPACE

#endif
