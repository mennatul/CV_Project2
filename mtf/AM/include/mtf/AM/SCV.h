#ifndef SCV_H
#define SCV_H

#include "SSDBase.h"

#define SCV_N_BINS 256
#define SCV_USE_BSPL 0
#define SCV_POU 0
#define SCV_PRE_SEED 0
#define SCV_WEIGHTED_MAPPING 0
#define SCV_MAPPED_GRADIENT 0
#define SCV_DEBUG_MODE 0

_MTF_BEGIN_NAMESPACE

struct SCVParams : ImgParams{
	// use BSpline function of order 3 rather than the Dirac Delta function
	//! as the kernel function for Parzen density estimation
	bool use_bspl;
	//! no. of bins in the histograms
	//! if use_bspl and partition_of_unity are enabled, this should be 2 more than the desired no. of bins (w.r.t normalized pixel range)
	//! since the actual range within which the pixel values are normalized is 2 less than this value to avoid
	//!	boundary conditions while computing the contribution of each pixel to different bins by ensuring that pixels with the maximum and
	//! minimum values contribute to all 4 bins required by the bspline function of order 3 used here;
	int n_bins;
	//! decides whether the partition of unity constraint has to be strictly observed for border bins;
	//! if enabled, the pixel values will be normalized in the range [1, n_bins-2] so each pixel contributes to all 4 bins
	bool partition_of_unity;
	//! initial value with which each bin of the joint histogram is pre-seeded
	//! to avoid numerical instabilities due to empty or near empty bins
	double pre_seed;
	// enable this to map each intensity to the weighted average of the two entries of the intensity map corresponding
	// to the floor and ceil of that intensity; if disabled it will be mapped to the entry corresponding to its floor 
	// leading to some information loss due to the fractional part that was discarded
	bool weighted_mapping;
	// enable this to automatically update the initial pixel gradient using the latest intensity map 
	// whenever the current pixel gradient is updated assuming that the current pixel values and thus the intensity map must
	// have changed since the last time the initial pixel gradient was computed;
	bool mapped_gradient;
	//! decides whether logging data will be printed for debugging purposes; 
	//! only matters if logging option is enabled at compile time
	bool debug_mode;

	//! value constructor
	SCVParams(ImgParams *img_params,
		bool _use_bspl, int _n_bins, double _pre_seed,
		bool _partition_of_unity, bool _weighted_mapping, 
		bool _mapped_gradient, bool _debug_mode):
		ImgParams(img_params){
		use_bspl = _use_bspl;
		n_bins = _n_bins;
		pre_seed = _pre_seed;
		partition_of_unity = _partition_of_unity;
		weighted_mapping = _weighted_mapping;
		mapped_gradient = _mapped_gradient;
		debug_mode = _debug_mode;
		if (n_bins <= 0)
			n_bins = SCV_N_BINS;
	}
	//! default/copy constructor
	SCVParams(SCVParams *params = nullptr) :
		ImgParams(params),
		n_bins(SCV_N_BINS), 
		pre_seed(SCV_PRE_SEED),
		partition_of_unity(SCV_POU), 
		weighted_mapping(SCV_WEIGHTED_MAPPING),
		mapped_gradient(SCV_MAPPED_GRADIENT), 
		debug_mode(SCV_DEBUG_MODE){
		if (params){
			use_bspl = params->use_bspl;
			n_bins = params->n_bins;
			pre_seed = params->pre_seed;
			partition_of_unity = params->partition_of_unity;
			weighted_mapping = params->weighted_mapping;
			mapped_gradient = params->mapped_gradient;
			debug_mode = params->debug_mode;
			if(n_bins <= 0)
				n_bins = SCV_N_BINS;
		}
	}
};

//! Sum of Conditional Variance
class SCV : public SSDBase{
public:

	using AppearanceModel::updatePixHess;

	EigImgT init_img;
	Matrix3d init_warp;
	Matrix3Xd init_pts_hm;
	Matrix2Xd init_pts;
	VectorXd orig_init_pix_vals;
	Matrix8Xd init_warped_offset_pts;

	typedef SCVParams ParamType;
	ParamType params;

	double hist_pre_seed;

	VectorXd intensity_map;

	// let A = err_vec_size = n_bins*n_bins and N = n_pix = no. of pixels
	//! n_bins x n_bins joint histograms; 
	MatrixXd curr_joint_hist;
	VectorXd init_hist, curr_hist;
	MatrixXd init_hist_mat, curr_hist_mat;

	SCV(ParamType *scv_params);
	SCV() : SSDBase(), init_img(0, 0, 0){}

	void initializePixVals(const Matrix2Xd& init_pts) override;
	void initializePixGrad(const Matrix2Xd &init_pts) override;
	void initializePixGrad(const Matrix8Xd &warped_offset_pts) override;

	void updatePixVals(const Matrix2Xd& curr_pts) override;
	void updatePixGrad(const Matrix2Xd &curr_pts) override;
	void updatePixGrad(const Matrix8Xd &warped_offset_pts) override;

	void updatePixHess(const Matrix2Xd &curr_pts) override;

private:
	// only used internally to increase speed by offlining as many computations as possible;
	MatrixX2i _std_bspl_ids;
	MatrixX2i _init_bspl_ids;
	MatrixX2i _curr_bspl_ids;
	EigImgMat _init_img;
};

_MTF_END_NAMESPACE

#endif