#ifndef PCA_H
#define PCA_H

#include "SSDBase.h"

#define PCA_BASIS_DATA_ROOT_DIR "Datasets"
#define PCA_ACTOR "TMT"
#define PCA_SEQ_NAME "nl_mugII_s3"
#define PCA_EXTRACTION_ID 0

_MTF_BEGIN_NAMESPACE
struct PCAParams : ImgParams{
	string basis_data_root_dir;
	string actor;
	string seq_name;
	int extraction_id;
	//! value constructor
	PCAParams(ImgParams *img_params,
		string _basis_data_root_dir,
		string _actor,
		string _seq_name,
		int _extraction_id);
	//! default/copy constructor
	PCAParams(PCAParams *params = nullptr);
};

class PCA : public SSDBase{
public:

	typedef PCAParams ParamType;
	ParamType params;

	// Define feature vectors
	VectorXd eigen_feat;  // eigen coefficients/features
	MatrixXd eigen_patch; // eigen template
	int n_feat = 50; // number of features in the image patch (number of eigen basis)
	VectorXd init_feat, curr_feat; //! features in the object patch being tracked (flattened as a vector)
	VectorXd curr_feat_diff;
	double init_norm;
	MatrixXd eigen_transform; // B*B'
	VectorXd mean_img;

	PCA(ParamType *pca_params);
	PCA() : SSDBase(){}

	int getDistFeatSize() override{ return n_feat; }

	// Returns the feature vector as VectorXd
	const VectorXd& getCurrFeat(){ return eigen_feat; }

	// computes a "distance" vector using the current image patch
	void updateDistFeat() override;
	using SSDBase ::updateDistFeat;

	void initializeDistFeat() override{ n_feat = eigen_patch.cols(); }// initialize the number of features

	// Returns a pointer to the feature vector
	const double* getDistFeat() override;

	// Returns a normalized version of the similarity that lies between 0 and 1
	double getLikelihood() override;

	// Initialize the features
	void initialize() override;

	// Update the similarity
	void update(bool prereq_only=true) override;

	//void initializePixVals(const Matrix2Xd& init_pts) override;
	//void updatePixVals(const Matrix2Xd& curr_pts) override;


};

_MTF_END_NAMESPACE

#endif
