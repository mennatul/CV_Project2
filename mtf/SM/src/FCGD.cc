#include "mtf/SM/FCGD.h"
#include "mtf/Utilities/miscUtils.h"
#include <time.h>

_MTF_BEGIN_NAMESPACE

template <class AM, class SSM>
FCGD<AM, SSM >::FCGD(ParamType *fcgd_params,
	AMParams *am_params, SSMParams *ssm_params) :
	SearchMethod<AM, SSM>(am_params, ssm_params),
	params(fcgd_params) {

	printf("\n");
	printf("Initializing Forward Compositional Gradient Descent tracker with:\n");
	printf("max_iters: %d\n", params.max_iters);
	printf("epsilon: %f\n", params.epsilon);
	printf("learning_rate: %f\n", params.learning_rate);
	printf("debug_mode: %d\n", params.debug_mode);
	printf("hess_type: %d\n", params.hess_type);
	printf("appearance model: %s\n", am->name.c_str());
	printf("state space model: %s\n", ssm->name.c_str());
	printf("\n");

	name = "fcgd";
	log_fname = "log/mtf_fcgd_log.txt";
	time_fname = "log/mtf_fcgd_times.txt";
	frame_id = 0;

	printf("ssm->getStateSize(): %d\n", ssm->getStateSize());

	ssm_update.resize(ssm->getStateSize());
	curr_pix_jacobian.resize(am->getPixCount(), ssm->getStateSize());
	hessian.resize(ssm->getStateSize(), ssm->getStateSize());
	jacobian.resize(ssm->getStateSize());
	learning_rate = params.learning_rate;
	printf("learning_rate: %f\n", learning_rate);

}

template <class AM, class SSM>
void FCGD<AM, SSM >::initialize(const cv::Mat &corners){
#ifdef LOG_GD_TIMES
	INIT_TIMER(start_time);
#endif
	ssm->initialize(corners);
	am->initializePixVals(ssm->getPts());

	//am->initializePixGrad(ssm->getPts());

	ssm->initializeGradPts(am->getGradOffset());
	am->initializePixGrad(ssm->getGradPts());

	am->initialize();
	am->initializeGrad();
	am->initializeHess();
	ssm->getCorners(cv_corners_mat);

#ifdef LOG_GD_TIMES
	double init_time;
	END_TIMER(start_time, end_time, init_time);
	utils::printScalarToFile(init_time, "init_time", time_fname, "%15.9f", "w");
	utils::printScalarToFile("cmp_times", NULL, "log/ssm_cmp_times.txt", "%s", "w");
#endif
}

template <class AM, class SSM>
void FCGD<AM, SSM >::update(){
	++frame_id;
#ifdef LOG_GD_DATA
	if(params.debug_mode){		
		am->iter_id = 0;
}
#endif
#ifdef LOG_GD_TIMES
	utils::printScalarToFile(frame_id, "\n\nframe_id", time_fname, "%6d", "a");
	double upd_time = 0;
#endif
	am->setFirstIter();
	for(int i = 0; i < params.max_iters; i++){
#ifdef LOG_GD_TIMES
		proc_times.clear();
		proc_labels.clear();
		INIT_TIMER(start_time);
#endif
		am->updatePixVals(ssm->getPts());
#ifdef LOG_GD_TIMES
		RECORD_EVENT(start_time, end_time, "am->updatePixVals", proc_times, proc_labels);
#endif	
		/*		am->updatePixGrad(ssm->getPts());
		#ifdef LOG_GD_TIMES
		RECORD_EVENT(start_time, end_time, "am->updatePixGrad", proc_times, proc_labels);
		#endif*/

		ssm->updateGradPts(am->getGradOffset());
#ifdef LOG_FCLK_TIMES
		RECORD_EVENT(start_time, end_time, "ssm->updateGradPts", proc_times, proc_labels);
#endif	
		am->updatePixGrad(ssm->getGradPts());
#ifdef LOG_FCLK_TIMES
		RECORD_EVENT(start_time, end_time, "am->updatePixGrad New", proc_times, proc_labels);
#endif	

		ssm->cmptPixJacobian(curr_pix_jacobian, am->getCurrPixGrad());
#ifdef LOG_GD_TIMES
		RECORD_EVENT(start_time, end_time, "ssm->cmptPixJacobian", proc_times, proc_labels);
#endif
		am->update();
#ifdef LOG_GD_TIMES
		RECORD_EVENT(start_time, end_time, "am->update", proc_times, proc_labels);
#endif
		am->updateCurrGrad();
#ifdef LOG_GD_TIMES
		RECORD_EVENT(start_time, end_time, "am->updateCurrGrad", proc_times, proc_labels);
#endif
		am->cmptCurrJacobian(jacobian, curr_pix_jacobian);
#ifdef LOG_GD_TIMES
		RECORD_EVENT(start_time, end_time, "am->cmptCurrJacobian", proc_times, proc_labels);
#endif	
		am->cmptCurrHessian(hessian, curr_pix_jacobian);

		MatrixXd temp=(jacobian*jacobian.transpose()) / (jacobian*hessian*jacobian.transpose());
		learning_rate = temp(0, 0);

		utils::printMatrix(temp, "temp");
		utils::printScalar(learning_rate, "learning_rate");

		ssm_update = -learning_rate*jacobian.transpose();
#ifdef LOG_GD_TIMES
		RECORD_EVENT(start_time, end_time, "ssm_update", proc_times, proc_labels);
#endif	
		prev_corners = ssm->getCorners();

		//		ssm->additiveUpdate(ssm_update);
		//#ifdef LOG_GD_TIMES
		//		RECORD_EVENT(start_time, end_time, "ssm->additiveUpdate", proc_times, proc_labels);
		//#endif
		ssm->compositionalUpdate(ssm_update);
#ifdef LOG_FCLK_TIMES
		RECORD_EVENT(start_time, end_time, "ssm->compositionalUpdate", proc_times, proc_labels);
#endif
		double update_norm = (prev_corners - ssm->getCorners()).squaredNorm();
#ifdef LOG_GD_TIMES
		RECORD_EVENT(start_time, end_time, "update_norm", proc_times, proc_labels);
		MatrixXd iter_times(proc_times.size(), 2);
		for(int proc_id = 0; proc_id < proc_times.size(); proc_id++){
			iter_times(proc_id, 0) = proc_times[proc_id];
		}
		double total_iter_time = iter_times.col(0).sum();
		iter_times.col(1) = (iter_times.col(0) / total_iter_time) * 100;
		utils::printScalarToFile(i, "iteration", time_fname, "%6d", "a");
		utils::printMatrixToFile(iter_times, "iter_times", time_fname, "%15.9f", "a",
			"\t", "\n", &proc_labels[0]);
		utils::printScalarToFile(total_iter_time, "total_iter_time", time_fname, "%15.9f", "a");
		upd_time+=total_iter_time;
		Vector6d cmp_times;
		cmp_times << proc_times[1], proc_times[2], proc_times[3], proc_times[8], proc_times[9], proc_times[10];
		utils::printMatrixToFile(cmp_times.transpose(), NULL, "log/ssm_cmp_times.txt", "%15.9f", "a");
#endif
		if(update_norm < params.epsilon){
			if(params.debug_mode){
				printf("n_iters: %d\n", i + 1);
			}
			break;
		}
		am->clearFirstIter();
	}
	ssm->getCorners(cv_corners_mat);

}

_MTF_END_NAMESPACE

#include "mtf/Macros/register.h"
_REGISTER_TRACKERS(FCGD);
