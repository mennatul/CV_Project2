#ifndef NN_H
#define NN_H

#include "SearchMethod.h"

//#include <boost/random/linear_congruential.hpp>
//#include <boost/random/normal_distribution.hpp>
//#include <boost/random/variate_generator.hpp>
//#include <boost/generator_iterator.hpp>

#include <flann/flann.hpp>

#define NN_MAX_ITERS 10
#define NN_EPSILON 0.01
#define NN_CORNER_SIGMA_D 0.06
#define NN_CORNER_SIGMA_T 0.04
#define NN_PIX_SIGMA 0
#define NN_N_SAMPLES 1000
#define NN_N_TREES 6
#define NN_N_CHECKS 50
#define NN_INDEX_TYPE 1
#define NN_CONFIG_FILE "Config/nn.cfg"
#define NN_ADDITIVE_UPDATE 1
#define NN_DIRECT_SAMPLES 1
#define NN_SSM_SIGMA_PREC 1.1

#define NN_DEBUG_MODE false
#define NN_CORNER_GRAD_EPS 1e-5

_MTF_BEGIN_NAMESPACE

namespace NNIndex{
	enum class IdxType{
		KDTree, HierarchicalClustering, KMeans, Composite, Linear,
		KDTreeSingle, KDTreeCuda3d, Autotuned, SavedIndex
	};
	int kdt_trees = 6;
	int km_branching = 32;
	int km_iterations = 11;
	flann::flann_centers_init_t km_centers_init = flann::FLANN_CENTERS_RANDOM;
	float km_cb_index = 0.2;
	int kdts_leaf_max_size = 10;
	int kdtc_leaf_max_size = 64;
	int hc_branching = 32;
	flann::flann_centers_init_t hc_centers_init = flann::FLANN_CENTERS_RANDOM;
	int hc_trees = 4;
	int hc_leaf_max_size = 100;
	float auto_target_precision = 0.9;
	float auto_build_weight = 0.01;
	float auto_memory_weight = 0;
	float auto_sample_fraction = 0.1;
	char* saved_idx_fname = "nn_saved_idx.txt";

	const char* getIndexType(IdxType index_type);
	void processStringParam(char* &str_out, const char* param);
	void processAgrument(char *arg_name, char *arg_val);
	const flann::IndexParams getIndexParams(IdxType index_type,
		const char* param_fname = "Config/nn.cfg");

}

struct NNParams{
	int max_iters; //! maximum iterations of the NN algorithm to run for each frame
	int n_samples;
	double epsilon; //! maximum L2 norm of the state update vector at which to stop the iterations	
	
	vector<double> ssm_sigma;
	double corner_sigma_d;
	double corner_sigma_t;
	double pix_sigma;

	NNIndex::IdxType index_type;
	string index_config_file;
	int n_checks;

	bool additive_update;
	bool direct_samples;

	double ssm_sigma_prec; //! precision within which the change in corners produced by the change in
	// a parameter for the chosen sigma should match the desired change in corners
	bool debug_mode; //! decides whether logging data will be printed for debugging purposes; 
	//! only matters if logging is enabled at compile time

	NNParams(int _max_iters, int _n_samples, double _epsilon,
		const vector<double> &_ssm_sigma,
		double _corner_sigma_d, double _corner_sigma_t,
		double _pix_sigma,
		NNIndex::IdxType _index_type, 
		const string _index_config_file, int _n_checks,
		bool _additive_update, bool _direct_samples,
		double _ssm_sigma_prec, bool _debug_mode);
	NNParams(NNParams *params = nullptr);
};

template<class AM, class SSM>
class NN : public SearchMethod < AM, SSM > {
private:
	init_profiling();
	char *log_fname;
	char *time_fname;

	//PixGradT pix_grad_identity;
	//MatrixXd ssm_grad_norm;
	//RowVectorXd ssm_grad_norm_mean;
public:
	//typedef typename AM::DistanceMeasure DistanceMeasure;
	//DistanceMeasure dist_obj;

	//typedef boost::minstd_rand baseGeneratorType;
	//typedef boost::normal_distribution<> dustributionType;
	//typedef boost::variate_generator<baseGeneratorType&, dustributionType> randomGeneratorType;

	typedef flann::Matrix<double> flannMatT;
	typedef flann::Matrix<int> flannResultT;
	typedef flann::Index<AM> flannIdxT;
	//typedef flann::Index<flann::L2<double> > flannIdxT;

	typedef NNParams ParamType;
	ParamType params;

	using SearchMethod<AM, SSM> ::am;
	using SearchMethod<AM, SSM> ::ssm;
	using typename SearchMethod<AM, SSM> ::AMParams;
	using typename SearchMethod<AM, SSM> ::SSMParams;
	using SearchMethod<AM, SSM> ::cv_corners_mat;
	using SearchMethod<AM, SSM> ::name;
	using SearchMethod<AM, SSM> ::initialize;
	using SearchMethod<AM, SSM> ::update;

	int frame_id;
	int am_dist_size;
	int ssm_state_size;

	Matrix3d warp_update;
	VectorXd state_sigma;

	Matrix24d prev_corners;
	VectorXd ssm_update;	
	VectorXd inv_state_update;
	
	MatrixXd ssm_perturbations;

	MatrixXdr eig_dataset;
	VectorXd eig_query;
	VectorXi eig_result;
	VectorXd eig_dists;

	flannMatT *flann_dataset;
	flannMatT *flann_query;
	flannIdxT *flann_index;
	//flannResultT *flann_result;
	//flannMatT *flann_dists;

	int best_idx;
	double best_dist;

	NN(ParamType *nn_params = nullptr,
		AMParams *am_params = nullptr, SSMParams *ssm_params = nullptr);
	~NN(){
		delete(flann_dataset);
		delete(flann_query);
		delete(flann_index);
		//delete(flann_result);
		//delete(flann_dists);
	}

	void initialize(const cv::Mat &corners) override;
	void update() override;

	//double getCornerChangeNorm(const VectorXd &state_update);
	//double findSSMSigma(int param_id);
	//double findSSMSigmaGrad(int param_id);
};

_MTF_END_NAMESPACE

#endif

