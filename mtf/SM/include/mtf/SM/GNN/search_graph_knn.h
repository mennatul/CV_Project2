
#ifndef SEARCH_GRAPH_KNN_H
#define SEARCH_GRAPH_KNN_H

#include "utility.h"
#include "build_graph.h"
#include "memwatch.h"

namespace gnn{
	struct stats
	{
		double *graph_time;
		double *linsearch_time;
		double *ratio;
		double *accuracy;
		int *non_exacts;
		int *depths;
		int *dist_no;
		double avg_graph_time;
		double avg_linsearch_time;
		double avg_ratio;
		double avg_acc_knn;
		int avg_depths;
		int avg_dist_no;
	};

	inline int min(int a, int b){return a < b ? a : b;}

	//void search_graph_1nn(int *Xq, int *X, int NNs, int k);
	int *sample_X(struct node *Nodes, int node_ind, int n_dims, int *X);
	void pick_knns(struct indx_dist *vis_nodes, int visited, struct indx_dist **gnn_dists, int K, int *gnns_cap);
	struct indx_dist* intersect(struct indx_dist *gnns, struct indx_dist *tnns, int K, int *size);

	template<typename DistType>
	struct indx_dist *search_graph(struct node *Nodes, double *Xq, double *X,
		int n_samples, int n_dims,
		int NNs, int K, const DistType &dist_func){
		// NNs: graph_size

		//struct stats* Stat = allocate_stats();
		double *query = static_cast<double*>(malloc(n_dims*sizeof(double)));
		check_pointer(query, "Couldn't malloc query");

		struct indx_dist *tnn_dists, *gnn_dists, *knns, *visited_nodes;

		tnn_dists = static_cast<indx_dist*>(malloc(/*n_samples*/K * sizeof(struct indx_dist))); //true nn-dists
		check_pointer(tnn_dists, "Couldn't malloc tnn_dists");
		int gnns_cap = K; //NNs*3;
		gnn_dists = static_cast<indx_dist*>(malloc(gnns_cap * sizeof(struct indx_dist))); // graph nn-dists
		check_pointer(gnn_dists, "Couldn't malloc gnn_dists");

		int dist_cnt, depth, knns_size;

		for(int j = 0; j < n_dims; j++)
			query[j] = Xq[n_dims + j];

		tnn_dists = static_cast<indx_dist*>(realloc(tnn_dists, /*n_samples*/ K * sizeof(struct indx_dist)));
		check_pointer(tnn_dists, "Couldn't realloc tnn_dists");


		// knn_search(query, tnn_dists, X, n_samples, n_dims);
		knn_search2<DistType>(query, tnn_dists, X, n_samples, n_dims, K, dist_func);


		//tnn_dists = realloc(tnn_dists, K * sizeof(struct indx_dist)); // shrinks the size to K   
		check_pointer(tnn_dists, "Couldn't realloc tnn_dists");

		// Graph search time 
		int visited_cap = K * 4;  //avg depth = 4
		int visited = 0;   // number of visited nodes
		visited_nodes = static_cast<indx_dist*>(malloc(visited_cap * sizeof(struct indx_dist)));
		check_pointer(visited_nodes, "Couldn't malloc visited_nodes");

		int rand_node, r;
		double *x_row;
		double parent_dist, dd;

		depth = 0;
		dist_cnt = 0;
		rand_node = my_rand(0, n_samples - 1);
		r = rand_node;
		x_row = &X[r*n_dims]; //X+r*n_dims;
		parent_dist = dist_func(query, x_row, n_dims);
		// parent_dist = my_dist(query, x_row, n_dims);

		visited_nodes[0].idx = r;
		visited_nodes[0].dist = parent_dist;
		visited++;

		while(1)
		{
			//      X1 = sample_X(Nodes, r, X); //contains the neighbors of node r
			if(Nodes[r].size > gnns_cap) //Nodes[r].size != gnns_size)
			{
				gnns_cap = Nodes[r].size;
				gnn_dists = static_cast<indx_dist*>(realloc(gnn_dists, gnns_cap * sizeof(struct indx_dist)));
				check_pointer(gnn_dists, "Couldn't realloc dists");
			}

			// knn_search(query, gnn_dists, X1, Nodes[r].size, n_dims);
			// knn_search1(query, gnn_dists, X, Nodes[r].size, n_dims, Nodes[r].nns_inds);
			knn_search11<DistType>(query, gnn_dists, X, Nodes[r].size, n_dims, K, Nodes[r].nns_inds,
				dist_func);

			//      free(X1); 

			int m = min(K, Nodes[r].size);
			if((visited + m) > visited_cap)
			{
				do {
					visited_cap *= 2;
				} while(visited_cap < (visited + m));
				visited_nodes = static_cast<indx_dist*>(realloc(visited_nodes, visited_cap *sizeof(struct indx_dist)));
				check_pointer(visited_nodes, "Couldn't realloc visited_nodes");
			}
			for(int i = 0; i < m; i++)
			{
				visited_nodes[visited + i].idx = Nodes[r].nns_inds[gnn_dists[i].idx];
				visited_nodes[visited + i].dist = gnn_dists[i].dist;
			}
			visited = visited + m;

			if(parent_dist <= gnn_dists[0].dist)
				break;
			else
				dd = gnn_dists[0].dist;

			r = Nodes[r].nns_inds[gnn_dists[0].idx];
			depth++;
			dist_cnt += Nodes[r].size;
			x_row = &X[r*n_dims]; // X+r*n_dims
			// parent_dist = my_dist(query, x_row, n_dims);
			parent_dist = dist_func(query, x_row, n_dims);
		}
		//gnns_size = K;
		//gnn_dists = realloc(gnn_dists, gnns_size * sizeof(struct indx_dist));
		//check_pointer(gnn_dists, "Couldn't realloc gnn_dists");

		// Given visited_nodes and their dists, selects the first knns and puts into gnns_dists
		pick_knns(visited_nodes, visited, &gnn_dists, K, &gnns_cap);
		knns = intersect(gnn_dists, tnn_dists, K, &knns_size);


		free(query);
		free(gnn_dists);
		free(tnn_dists);
		return knns;
	}
}
#endif
