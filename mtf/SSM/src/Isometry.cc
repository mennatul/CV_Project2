#include "mtf/SSM/Isometry.h"
#include "mtf/Utilities/warpUtils.h"
#include "mtf/Utilities/miscUtils.h"
#define _USE_MATH_DEFINES
#include <math.h>

_MTF_BEGIN_NAMESPACE

Isometry::Isometry(int resx, int resy, IsometryParams *params_in) : ProjectiveBase(resx, resy),
params(params_in){

	printf("\n");
	printf("initializing Isometry state space model with:\n");
	printf("resx: %d\n", resx);
	printf("resy: %d\n", resy);
	printf("c: %f\n", params.c);
	printf("debug_mode: %d\n", params.debug_mode);

	name = "isometry";
	state_size = 3;
	curr_state.resize(state_size);

	//new (&affine_warp_mat) Map<Matrix23d>(curr_warp.topRows(2).data()); 
}

void Isometry::compositionalUpdate(const VectorXd& state_update){
	VALIDATE_ISO_STATE(state_update);

	getWarpFromState(warp_update_mat, state_update);
	curr_warp = curr_warp * warp_update_mat;

	getStateFromWarp(curr_state, curr_warp);

	curr_pts.noalias() = curr_warp.topRows<2>() * init_pts_hm;
	curr_corners.noalias() = curr_warp.topRows<2>() * init_corners_hm;
}

void Isometry::getWarpFromState(Matrix3d &warp_mat,
	const VectorXd& ssm_state){
	VALIDATE_ISO_STATE(ssm_state);

	double tx = ssm_state(0);
	double ty = ssm_state(1);
	double cos_theta = cos(ssm_state(2));
	double sin_theta = sin(ssm_state(2));

	warp_mat(0, 0) = cos_theta;
	warp_mat(0, 1) = -sin_theta;
	warp_mat(0, 2) = tx;
	warp_mat(1, 0) = sin_theta;
	warp_mat(1, 1) = cos_theta;
	warp_mat(1, 2) = ty;
	warp_mat(2, 0) = 0;
	warp_mat(2, 1) = 0;
	warp_mat(2, 2) = 1;
}

void Isometry::getStateFromWarp(VectorXd &state_vec,
	const Matrix3d& iso_mat){
	VALIDATE_ISO_STATE(state_vec);
	VALIDATE_ISO_WARP(iso_mat);

	state_vec(0) = iso_mat(0, 2);
	state_vec(1) = iso_mat(1, 2);
	state_vec(2) = getAngleOfRotation(iso_mat(1, 0), iso_mat(0, 0));
}

void Isometry::cmptInitPixJacobian(MatrixXd &jacobian_prod,
	const PixGradT &pix_jacobian){
	VALIDATE_SSM_JACOBIAN(jacobian_prod, pix_jacobian);

	for(int i = 0; i < n_pts; i++){
		double x = init_pts(0, i);
		double y = init_pts(1, i);
		double Ix = pix_jacobian(i, 0);
		double Iy = pix_jacobian(i, 1);

		jacobian_prod(i, 0) = Ix;
		jacobian_prod(i, 1) = Iy;
		jacobian_prod(i, 2) = Iy*x - Ix*y;
	}
}

void Isometry::cmptPixJacobian(MatrixXd &jacobian_prod,
	const PixGradT &pix_jacobian){
	VALIDATE_SSM_JACOBIAN(jacobian_prod, pix_jacobian);

	for(int i = 0; i < n_pts; i++){
		//double x = init_pts(0, i);
		//double y = init_pts(1, i);

		double curr_x = curr_pts(0, i);
		double curr_y = curr_pts(1, i);

		double Ix = pix_jacobian(i, 0);
		double Iy = pix_jacobian(i, 1);

		jacobian_prod(i, 0) = Ix;
		jacobian_prod(i, 1) = Iy;
		//jacobian_prod(i, 2) = Iy*(x*cos_theta - y*sin_theta) - Ix*(x*sin_theta + y*cos_theta);
		jacobian_prod(i, 2) = Iy*curr_x - Ix*curr_y;
	}
}

void Isometry::getInitPixGrad(Matrix2Xd &ssm_grad, int pt_id) {
	double x = init_pts(0, pt_id);
	double y = init_pts(1, pt_id);
	ssm_grad <<
		1, 0,  -y,
		0, 1,  x;
}

void Isometry::getCurrPixGrad(Matrix2Xd &ssm_grad, int pt_id) {
	double curr_x = curr_pts(0, pt_id);
	double curr_y = curr_pts(1, pt_id);
	ssm_grad <<
		1, 0, -curr_y,
		0, 1, curr_x;
}

void Isometry::cmptInitPixHessian(MatrixXd &pix_hess_ssm, const PixHessT &pix_hess_coord,
	const PixGradT &pix_grad){
	VALIDATE_SSM_HESSIAN(pix_hess_ssm, pix_hess_coord, pix_grad);

	Matrix23d ssm_grad;
	for(int pt_id = 0; pt_id < n_pts; pt_id++){
		double x = init_pts(0, pt_id);
		double y = init_pts(1, pt_id);
		ssm_grad <<
			1, 0, -y,
			0, 1, x;
		Map<Matrix3d> curr_pix_hess_ssm((double*)pix_hess_ssm.col(pt_id).data());
		curr_pix_hess_ssm = ssm_grad.transpose()*Map<Matrix2d>((double*)pix_hess_coord.col(pt_id).data())*ssm_grad;
	}
}

void Isometry::cmptPixHessian(MatrixXd &pix_hess_ssm, const PixHessT &pix_hess_coord,
	const PixGradT &pix_grad){
	VALIDATE_SSM_HESSIAN(pix_hess_ssm, pix_hess_coord, pix_grad);

	Matrix23d ssm_grad;
	for(int pt_id = 0; pt_id < n_pts; pt_id++){
		double Ix = pix_grad(pt_id, 0);
		double Iy = pix_grad(pt_id, 1);

		double curr_x = curr_pts(0, pt_id);
		double curr_y = curr_pts(1, pt_id);
		ssm_grad <<
			1, 0, -curr_y,
			0, 1, curr_x;
		Map<Matrix3d> curr_pix_hess_ssm((double*)pix_hess_ssm.col(pt_id).data());
		curr_pix_hess_ssm = ssm_grad.transpose()*Map<Matrix2d>((double*)pix_hess_coord.col(pt_id).data())*ssm_grad;
		curr_pix_hess_ssm(2, 2) += Iy*curr_y - Ix*curr_x;
	}
}

void Isometry::cmptApproxPixJacobian(MatrixXd &jacobian_prod,
	const PixGradT &pix_jacobian) {
	VALIDATE_SSM_JACOBIAN(jacobian_prod, pix_jacobian);
	double cos_theta = curr_warp(0, 0);
	double sin_theta = curr_warp(1, 0);

	for(int i = 0; i < n_pts; i++){
		double x = init_pts(0, i);
		double y = init_pts(1, i);

		double Ix = pix_jacobian(i, 0);
		double Iy = pix_jacobian(i, 1);

		jacobian_prod(i, 0) = Ix*cos_theta - Iy*sin_theta;
		jacobian_prod(i, 1) = Ix*sin_theta + Iy*cos_theta;
		jacobian_prod(i, 2) = Iy*x - Ix*y;
	}
}

void Isometry::estimateWarpFromCorners(VectorXd &state_update, const Matrix24d &in_corners,
	const Matrix24d &out_corners){
	VALIDATE_SSM_STATE(state_update);

	Matrix3d warp_update_mat = utils::computeSimilitudeDLT(in_corners, out_corners);
	double a_plus_1 = warp_update_mat(0, 0);
	double b = warp_update_mat(1, 0);

	double s_plus_1 = sqrt(a_plus_1*a_plus_1 + b*b);
	double cos_theta = a_plus_1 / s_plus_1;
	double sin_theta = b / s_plus_1;

	double theta = getAngleOfRotation(sin_theta, cos_theta);

	//double cos_theta_final = cos(theta);
	//double sin_theta_final = sin(theta);
	//if(cos_theta_final != sin_theta_final){
	//	printf("cos and sin theta are not same for theta:  %15.9f : \n", theta);
	//	printf("cos_theta: %15.9f\n", cos_theta_final);
	//	printf("sin_theta: %15.9f\n", sin_theta_final);
	//}

	state_update(0) = warp_update_mat(0, 2);
	state_update(1) = warp_update_mat(1, 2);
	state_update(2) = theta;
}

double Isometry::getAngleOfRotation(double sin_theta, double cos_theta){
	double theta = 0;
	if(cos_theta < 0){
		if(sin_theta < 0){// 3rd quadrant
			return 2 * M_PI - acos(cos_theta);
		} else{// 2nd quadrant
			return acos(cos_theta);
		}
	} else{// 1st or 4th quadrant

		return asin(sin_theta);
	}
	
}

void Isometry::updateGradPts(double grad_eps){
	Vector2d diff_vec_x_warped = curr_warp.topRows<2>().col(0) * grad_eps;
	Vector2d diff_vec_y_warped = curr_warp.topRows<2>().col(1) * grad_eps;

	for(int pt_id = 0; pt_id < n_pts; pt_id++){
		grad_pts(0, pt_id) = curr_pts(0, pt_id) + diff_vec_x_warped(0);
		grad_pts(1, pt_id) = curr_pts(1, pt_id) + diff_vec_x_warped(1);

		grad_pts(2, pt_id) = curr_pts(0, pt_id) - diff_vec_x_warped(0);
		grad_pts(3, pt_id) = curr_pts(1, pt_id) - diff_vec_x_warped(1);

		grad_pts(4, pt_id) = curr_pts(0, pt_id) + diff_vec_y_warped(0);
		grad_pts(5, pt_id) = curr_pts(1, pt_id) + diff_vec_y_warped(1);

		grad_pts(6, pt_id) = curr_pts(0, pt_id) - diff_vec_y_warped(0);
		grad_pts(7, pt_id) = curr_pts(1, pt_id) - diff_vec_y_warped(1);
	}
}


void Isometry::updateHessPts(double hess_eps){
	double hess_eps2 = 2 * hess_eps;

	Vector2d diff_vec_xx_warped = curr_warp.topRows<2>().col(0) * hess_eps2;
	Vector2d diff_vec_yy_warped = curr_warp.topRows<2>().col(1) * hess_eps2;
	Vector2d diff_vec_xy_warped = (curr_warp.topRows<2>().col(0) + curr_warp.topRows<2>().col(1)) * hess_eps;
	Vector2d diff_vec_yx_warped = (curr_warp.topRows<2>().col(0) - curr_warp.topRows<2>().col(1)) * hess_eps;

	for(int pt_id = 0; pt_id < n_pts; pt_id++){

		hess_pts(0, pt_id) = curr_pts(0, pt_id) + diff_vec_xx_warped(0);
		hess_pts(1, pt_id) = curr_pts(1, pt_id) + diff_vec_xx_warped(1);

		hess_pts(2, pt_id) = curr_pts(0, pt_id) - diff_vec_xx_warped(0);
		hess_pts(3, pt_id) = curr_pts(1, pt_id) - diff_vec_xx_warped(1);

		hess_pts(4, pt_id) = curr_pts(0, pt_id) + diff_vec_yy_warped(0);
		hess_pts(5, pt_id) = curr_pts(1, pt_id) + diff_vec_yy_warped(1);

		hess_pts(6, pt_id) = curr_pts(0, pt_id) - diff_vec_yy_warped(0);
		hess_pts(7, pt_id) = curr_pts(1, pt_id) - diff_vec_yy_warped(1);

		hess_pts(8, pt_id) = curr_pts(0, pt_id) + diff_vec_xy_warped(0);
		hess_pts(9, pt_id) = curr_pts(1, pt_id) + diff_vec_xy_warped(1);

		hess_pts(10, pt_id) = curr_pts(0, pt_id) - diff_vec_xy_warped(0);
		hess_pts(11, pt_id) = curr_pts(1, pt_id) - diff_vec_xy_warped(1);

		hess_pts(12, pt_id) = curr_pts(0, pt_id) + diff_vec_yx_warped(0);
		hess_pts(13, pt_id) = curr_pts(1, pt_id) + diff_vec_yx_warped(1);

		hess_pts(14, pt_id) = curr_pts(0, pt_id) - diff_vec_yx_warped(0);
		hess_pts(15, pt_id) = curr_pts(1, pt_id) - diff_vec_yx_warped(1);
	}
}

void Isometry::setState(const VectorXd &ssm_state){
	VALIDATE_SSM_STATE(ssm_state);
	curr_state = ssm_state;
	getWarpFromState(curr_warp, curr_state);
	curr_pts.noalias() = curr_warp.topRows<2>() * init_pts_hm;
	curr_corners.noalias() = curr_warp.topRows<2>() * init_corners_hm;
}

void Isometry::applyWarpToCorners(Matrix24d &warped_corners, const Matrix24d &orig_corners,
	const VectorXd &ssm_state){
	getWarpFromState(warp_mat, ssm_state);
	for(int i = 0; i < 4; i++){
		warped_corners(0, i) = warp_mat(0, 0)*orig_corners(0, i) + warp_mat(0, 1)*orig_corners(1, i) +
			warp_mat(0, 2);
		warped_corners(1, i) = warp_mat(1, 0)*orig_corners(0, i) + warp_mat(1, 1)*orig_corners(1, i) +
			warp_mat(1, 2);
	}
}

void Isometry::applyWarpToPts(Matrix2Xd &warped_pts, const Matrix2Xd &orig_pts,
	const VectorXd &ssm_state){
	getWarpFromState(warp_mat, ssm_state);
	int n_pts = orig_pts.cols();
	for(int i = 0; i < n_pts; i++){
		warped_pts(0, i) = warp_mat(0, 0)*orig_pts(0, i) + warp_mat(0, 1)*orig_pts(1, i) +
			warp_mat(0, 2);
		warped_pts(1, i) = warp_mat(1, 0)*orig_pts(0, i) + warp_mat(1, 1)*orig_pts(1, i) +
			warp_mat(1, 2);
	}
}

_MTF_END_NAMESPACE

