#ifndef HOMOGRAPHY_H
#define HOMOGRAPHY_H

#define HOM_DIRECT_SAMPLES 0
#define HOM_NORMALIZED_BASIS false
#define HOM_DEBUG_MODE 0

#include "ProjectiveBase.h"

_MTF_BEGIN_NAMESPACE

struct HomographyParams{
	// use normalized unit square points/corners as the initial points/corners
	bool normalized_init;	
	// generate samples by ading perturbations directly to the corners
	// and using DLT method to estimate the corresponding warp
	bool corner_based_sampling;
	bool debug_mode;

	HomographyParams(bool _normalized_init, 
		bool _corner_based_sampling, bool _debug_mode){
		normalized_init = _normalized_init;
		corner_based_sampling = _corner_based_sampling;
		debug_mode = _debug_mode;
	}
	HomographyParams(HomographyParams *params = nullptr) :
		normalized_init(HOM_NORMALIZED_BASIS),
		corner_based_sampling(HOM_DIRECT_SAMPLES),
		debug_mode(HOM_DEBUG_MODE){
		if(params){
			corner_based_sampling = params->corner_based_sampling;
			normalized_init = params->normalized_init;
			debug_mode = params->debug_mode;
		}
	}
};

class Homography : public ProjectiveBase{
public:
	
	typedef HomographyParams ParamType;
	ParamType params;

	using ProjectiveBase::setCorners;

	Homography(int resx, int resy,HomographyParams *params_in=nullptr);
	void compositionalUpdate(const VectorXd& state_update) override;

	void cmptInitPixJacobian(MatrixXd &jacobian_prod, const PixGradT &am_jacobian) override;
	void cmptPixJacobian(MatrixXd &jacobian_prod, const PixGradT &am_jacobian) override;
	void cmptWarpedPixJacobian(MatrixXd &jacobian_prod,
		const PixGradT &pix_jacobian) override;

	void cmptApproxPixJacobian(MatrixXd &jacobian_prod,
		const PixGradT &pix_jacobian) override;
	void estimateWarpFromCorners(VectorXd &state_update, const CornersT &in_corners,
		const CornersT &out_corners) override;
	void cmptInitPixHessian(MatrixXd &pix_hess_ssm, const PixHessT &pix_hess_coord,
		const PixGradT &pix_grad) override;

	void cmptWarpedPixHessian(MatrixXd &pix_hess_ssm, const PixHessT &pix_hess_coord,
		const PixGradT &pix_grad) override;
	void cmptApproxPixHessian(MatrixXd &pix_hess_ssm, const PixHessT &pix_hess_coord,
		const PixGradT &pix_grad) override;
	void cmptPixHessian(MatrixXd &pix_hess_ssm, const PixHessT &pix_hess_coord,
		const PixGradT &pix_grad) override;

	void setCorners(const CornersT& corners) override;
	void estimateWarpFromPts(VectorXd &state_update, vector<uchar> &mask,
		const vector<cv::Point2f> &in_pts, const vector<cv::Point2f> &out_pts,
		int estimation_method, double ransac_reproj_thresh) override;

	void invertState(VectorXd& inv_state, const VectorXd& state) override;

	void updateGradPts(double grad_eps) override;
	void updateHessPts(double hess_eps) override;

	bool supportsSPI() override{ return true; }

	void getInitPixGrad(Matrix2Xd &jacobian_prod, int pix_id) override;
	void getCurrPixGrad(Matrix2Xd &jacobian_prod, int pix_id) override;

	void generatePerturbation(VectorXd &state_update) override;

private:
	CornersT rand_d;
	Vector2d rand_t;
	CornersT disturbed_corners;

	void getWarpFromState(Matrix3d &warp_mat, const VectorXd& ssm_state) override;
	void getStateFromWarp(VectorXd &state_vec, const Matrix3d& warp_mat) override;

};

_MTF_END_NAMESPACE

#endif
